const webpack = require('webpack')
const path = require('path')
const DIST = path.resolve(__dirname, 'dist/')
const SRC = path.resolve(__dirname, 'src/')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const VisualizerPlugin = require('webpack-visualizer-plugin')

const config = {
    devtool: 'source-maps',
    entry: {
        main: SRC + '/index.js'
    },
    output: {
        path: DIST,
        publicPath: '/dist',
        filename: 'index.js',
        library: ['tut-d3'],
        libraryTarget: 'umd'
    },
    resolve: {
        modules: [
            path.resolve('./'),
            path.resolve('./src'),
            path.resolve('./node_modules')
        ]
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.(css|scss)$/,
                loaders: [
                    'style-loader', 'css-loader', 'sass-loader'
                ],
                exclude: /node_modules/
            }, {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[ext]'
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: './src/styles/index.scss',
                to: './index.scss'
            }, {
                from: './src/styles',
                to: './scss'
            }
        ]),
        new VisualizerPlugin({
            filename: './bundle-size.html'
        })
    ],
    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
    // node: {     fs: 'empty' }
}

module.exports = config
