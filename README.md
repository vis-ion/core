# vis-ion core

[![](https://gitlab.com/vis-ion/core/badges/master/pipeline.svg)](http://vis-ion.gitlab.io/core/pipelines)

## [![](./source/img/ss1.png)](http://vis-ion.gitlab.io/core)

## Chart Types

Flat (Nested) Array data
### Bar
- [Bar Chart (Vertical)](https://observablehq.com/@d3/bar-chart)
- [Bar Chart (Horizontal)](https://observablehq.com/@d3/horizontal-bar-chart)
- [Grouped Bar (Vertical)](https://observablehq.com/@d3/grouped-bar-chart)
- [Stacked Bar (Vertical)](https://observablehq.com/@d3/stacked-bar-chart)
- [Stacked Bar (Horizontal)](https://observablehq.com/@d3/stacked-horizontal-bar-chart)
- [Stacked & Grouped](https://observablehq.com/@d3/stacked-to-grouped-bars)

### Line
- [Default](https://observablehq.com/@d3/line-chart)
- [MultiLine]()
- [CandleStick]()

### Area
- [Area]()
- [Stacked Area]()
- [SteamGraph]()

### Scatter
### Pie
### Radar

### Sunburst
- [Zoomable](https://observablehq.com/@d3/zoomable-sunburst)
- [Sequences](https://observablehq.com/@kerryrodden/sequences-sunburst)
- [Icicle](https://observablehq.com/@d3/zoomable-icicle)

## Utils

### Gradient
### Tooltip
