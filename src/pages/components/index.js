import React, { Component, Fragment } from 'react'
import FAIcon from 'react-fontawesome'
import {
    DocIndex,
    Markdown,
    Layout,
    Accordion,
    ActionButtons,
    Async,
    AutoComplete,
    Card,
    CardStack,
    Checkbox,
    Button,
    Dropdown,
    EditInline,
    ErrorBoundary,
    Input,
    JsonInput,
    Modal,
    Progress,
    Switch,
    Table,
    Tabs,
    Toaster,
    Tree
} from 'components'

// #region Misc
import { emojis, getEmoji } from '../../util/emoji'
import {
    collections,
    dropdownOptionsDefault,
    treeData
} from './mock'

export class DemoComponents extends Component {
    state = {
        checkbox1: false,
        checkbox2: true,
        checkbox3: false,
        switch1: false,
        switch2: true,
        switch3: false,
        isModalOpen: false,
        modalType: 'success',
        emojiQuery: 'face',
        emojisFiltered: Object.keys(emojis)
    }

    componentWillMount () {
        this.filterEmojis('face')
    }

    toggle = field => () => {
        this.setState({
            [field]: !this.state[field]
        })
    }

    setChartType = e => {
        this.setState({
            chartType: e.target.value
        })
    }

    componentDidMount () {
    }

    onChange = e => {
    }

    addToast = (obj) => {
        this.setState({
            toast: obj
        })
    }

    filterEmojis = (q) => {
        this.setState({
            emojiQuery: q,
            emojisFiltered: q.length > 0
                ? Object.keys(emojis)
                    .filter(emoji => emoji.includes(q))
                : Object.keys(emojis)
        }, () => console.log({ q }, this.state.emojisFiltered))
    }

    render () {
        const {
            checkbox1,
            checkbox2,
            checkbox3,
            switch1,
            switch2,
            switch3,
            isModalOpen,
            modalType,
            emojiQuery,
            emojisFiltered,
            toast
        } = this.state
        return <div className='page'>
            <Layout
                leftDiv={
                    <DocIndex
                        headerQuery={'#demo>div>div>span>span>span.section'}
                        footerQuery={'#demo>div>.section-end.divider.horizontal'}
                    />
                }
                rightDiv={
                    <div id='demo' className='page'>
                        <h2 className="padded">Components</h2>
                        <div className='divider horizontal' />
                        <Accordion isOpenDefault title={ <span className='section'>Accordion</span> }>
                            <div className='divider horizontal' />
                            <div style={{ backgroundColor: '#ffddee' }}>
                                <Accordion title={ 'Accordion: click to toggle visibility of contents' }>
                                    These are the contents of the accordion.
                                </Accordion>
                            </div>
                            <div style={{ backgroundColor: '#ccffff' }}>
                                <Accordion title={ 'Accordion: click to toggle visibility of contents' }>
                                    These are the contents of the accordion.
                                </Accordion>
                            </div>
                            <div style={{ backgroundColor: '#eeddff' }}>
                                <Accordion isOpenDefault title={ 'Accordion: isOpenDefault' }>
                                    This content is visible by default
                                </Accordion>
                            </div>
                        </Accordion>
                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Async</span> }>
                            <div className='divider horizontal' />
                            <Card style={{ width: '100%', height: '8rem' }}>
                                <Async isLoading>
                                This content shouldn't show... you should see a spinner.
                                </Async>
                            </Card>
                            <Card style={{ width: '100%', height: '8rem' }}>
                                <Async>
                                    <h4>This should be readable (isLoading is false)</h4>
                                </Async>
                            </Card>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>AutoComplete</span> }>
                            <div className='full-width'>
                                <AutoComplete
                                    options={[
                                        { name: 'hello' },
                                        { name: 'world' },
                                        { name: 'foobar' },
                                        { name: 'alice' },
                                        { name: 'bob' }
                                    ]}
                                />
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Button</span> }>
                            <div className='padded'>
                                <h4>Default</h4>
                                <div className='flex-row'>
                                    <Button color='primary'>Primary</Button>
                                    <Button color='secondary'>Secondary</Button>
                                    <Button color='success'>Success</Button>
                                    <Button color='warning'>Warning</Button>
                                    <Button color='error'>Error</Button>
                                </div>
                                <h4>Hollow</h4>
                                <div className='flex-row'>
                                    <Button className='primary hollow'>Primary</Button>
                                    <Button className='secondary hollow'>Secondary</Button>
                                    <Button className='success hollow'>Success</Button>
                                    <Button className='warning hollow'>Warning</Button>
                                    <Button className='error hollow'>Error</Button>
                                </div>
                                <h4>Round</h4>
                                <div className='flex-row'>
                                    <Button className='primary round'>Primary</Button>
                                    <Button className='secondary round'>Secondary</Button>
                                    <Button className='success round'>Success</Button>
                                    <Button className='warning round'>Warning</Button>
                                    <Button className='error round'>Error</Button>
                                </div>
                                <h4>ActionButtons</h4>
                                <ActionButtons
                                    onSave={() => alert('onSave')}
                                    onDelete={() => alert('onDelete')}
                                    onCancel={() => alert('onCancel')}
                                />
                            </div>
                        </Accordion>
                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Card</span> }>
                            <div className='padded'>
                                <div className='flex-row flex-wrap'>

                                    <Card
                                        width={'12rem'}
                                        height={'18rem'}
                                        margin={'0 0.25rem 0.25rem 0'}
                                    >
                                        <p>A card with a random paragraph worthy of Lorem ipsum</p>
                                    </Card>
                                    <Card
                                        width={'12rem'}
                                        height={'18rem'}
                                        margin={'0 0.25rem 0.25rem 0'}
                                    >
                                        <img
                                            src={'https://ak.picdn.net/shutterstock/videos/31215259/thumb/12.jpg'}
                                            style={{ width: '100%', height: '6rem', position: 'absolute' }}
                                        />
                                        <p style={{ paddingTop: '6rem' }}>A Card with an image</p>
                                    </Card>
                                    <Card
                                        width={'12rem'}
                                        height={'18rem'}
                                        margin={'0 0.25rem 0.25rem 0'}
                                    >
                                        <img
                                            src={'https://smallbizclub.com/wp-content/uploads/2018/04/Influencer-Marketing-and-Artificial-Intelligence.jpg'}
                                            style={{ width: '100%', height: '6rem', position: 'absolute' }}
                                        />
                                        <p style={{ paddingTop: '6rem' }}>Another Card with an image, and a slightly longer description.</p>
                                    </Card>
                                    <Card
                                        width={'12rem'}
                                        height={'18rem'}
                                        margin={'0 0.25rem 0.25rem 0'}
                                    >
                                        <img
                                            src={'https://smallbizclub.com/wp-content/uploads/2018/04/Influencer-Marketing-and-Artificial-Intelligence.jpg'}
                                            style={{ width: '100%', height: '6rem', position: 'absolute' }}
                                        />
                                        <p style={{ paddingTop: '6rem' }}>Another Card with an image, and a slightly longer description.</p>
                                        <div style={{ position: 'absolute', bottom: 0, width: '100%' }}>
                                            <div className='divider horizontal' />
                                            <div className='flex-row space-between' style={{ padding: '0.5rem' }}>
                                                {/* <div>{(collection.items).length}</div> */}
                                                <div>{' '}</div>
                                                <div style={{ paddingRight: '0.25rem' }}><FAIcon name='ellipsis-v' /></div>
                                            </div>
                                        </div>
                                    </Card>
                                    <Card
                                        width={'12rem'}
                                        height={'18rem'}
                                        margin={'0 0.25rem 0.25rem 0'}
                                    >
                                        <img
                                            src={'https://smallbizclub.com/wp-content/uploads/2018/04/Influencer-Marketing-and-Artificial-Intelligence.jpg'}
                                            style={{ width: '100%', height: '6rem', position: 'absolute' }}
                                        />
                                        <p style={{ paddingTop: '6rem' }}>Another Card with an image, and a slightly longer description.</p>
                                        <div style={{ position: 'absolute', bottom: 0, width: '100%' }}>
                                            <div className='divider horizontal' />
                                            <div className='flex-row space-between' style={{ padding: '0.5rem' }}>
                                                {/* <div>{(collection.items).length}</div> */}
                                                <div>{' '}</div>
                                                <div style={{ paddingRight: '0.25rem' }}><FAIcon name='ellipsis-v' /></div>
                                            </div>
                                        </div>
                                    </Card>
                                    <Card
                                        width={'12rem'}
                                        height={'18rem'}
                                        margin={'0 0.25rem 0.25rem 0'}
                                    >
                                        <img
                                            src={'https://smallbizclub.com/wp-content/uploads/2018/04/Influencer-Marketing-and-Artificial-Intelligence.jpg'}
                                            style={{ width: '100%', height: '6rem', position: 'absolute' }}
                                        />
                                        <p style={{ paddingTop: '6rem' }}>Another Card with an image, and a slightly longer description.</p>

                                        <div style={{ position: 'absolute', bottom: 0, width: '100%' }}>
                                            <div className='divider horizontal' />
                                            <div className='flex-row space-between' style={{ padding: '0.5rem' }}>
                                                {/* <div>{(collection.items).length}</div> */}
                                                <div>{' '}</div>
                                                <div style={{ paddingRight: '0.25rem' }}><FAIcon name='ellipsis-v' /></div>
                                            </div>
                                        </div>
                                    </Card>
                                </div>
                            </div>
                        </Accordion>
                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>CardStack</span> }>
                            <div className="flex-row flex-wrap padded">
                                {
                                    collections.map((collection, i) => (
                                        <CardStack
                                            isAsync
                                            width={'12rem'}
                                            height={'18rem'}
                                            margin='0 0.75rem 0.75rem 0'
                                            items={ collection.items }
                                            // isSeparate={ collection.items.length > 5}
                                            renderItem={(item) => <Fragment>
                                                <div style={{ position: 'relative', backgroundColor: 'white' }}>
                                                    <img src={item.url} style={{ width: '100%', height: '6rem', position: 'absolute' }}/>
                                                    <div style={{ paddingTop: '6rem' }}>
                                                        <div style={{ padding: '0.25rem' }}><strong>{ item.name }</strong></div>
                                                        <div style={{ padding: '0.25rem' }}>{ item.text }</div>

                                                    </div>
                                                </div>
                                            </Fragment>}
                                        >
                                            <div style={{ padding: '1rem' }}>
                                                <div ><b>{collection.name}</b></div>
                                                <div >{collection.text}</div>
                                            </div>
                                            <div style={{ position: 'absolute', bottom: 0, width: '100%' }}>
                                                <div className='divider horizontal' />
                                                <div className='flex-row space-between' style={{ padding: '0.5rem' }}>
                                                    <div>{(collection.items).length}</div>
                                                    <div style={{ paddingRight: '0.25rem' }}><FAIcon name='ellipsis-v' /></div>
                                                </div>
                                            </div>
                                        </CardStack>

                                    ))
                                }
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Checkbox</span> }>
                            <div className='padded'>
                                <Checkbox
                                    label='Checkbox 1'
                                    checked={ checkbox1 }
                                    onChange={ this.toggle('checkbox1') }
                                />
                                <Checkbox
                                    label='Checkbox 2'
                                    checked={ checkbox2 }
                                    onChange={ this.toggle('checkbox2') }
                                />
                                <Checkbox
                                    label='Checkbox 3'
                                    checked={ checkbox3 }
                                    onChange={ this.toggle('checkbox3') }
                                />
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>DateTime</span> }>
                            <div className='padded'></div>
                        </Accordion>
                        <div className='section-end' />

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Dropdown</span> }>
                            <div className='padded'>
                                <h4>Default</h4>
                                <Dropdown
                                    options={dropdownOptionsDefault}
                                />
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>EditInline</span> }>
                            <div className='padded'>
                                <EditInline value={'Each line here is an independant div'} />
                                <EditInline value={'That you can edit thanks to content-editable'} />
                                <EditInline value={'Go ahead, give it a try .. maybe even with an emoji 😉'} />
                                <EditInline value={'Hover to start editing'} />
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Emoji</span> }>
                            <input value={emojiQuery} onChange={ e => this.filterEmojis(e.target.value) } />
                            <div className="flex-row flex-wrap">
                                {
                                    emojisFiltered.map(key => <div
                                        className='card-1 flex-col space-between'
                                        style={{ width: '10rem', height: '5rem', margin: '1rem', textAlign: 'center', padding: '1rem' }}>
                                        <div style={{ fontSize: '2rem' }}>{getEmoji(key)}</div>
                                        <div>{ key }</div>
                                    </div>)
                                }
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Input</span> }>
                            <div className='padded'>
                                <div>
                                    <Input className='primary' type='text' defaultValue={ 'This is sample text' }/>
                                    <Input className='success' type='number' defaultValue={ '0123456789' }/>
                                    <Input className='warning' type='email' defaultValue={'test@email.com'}/>
                                    <Input className='error' type='password' defaultValue={'puts_the_lotion'}/>
                                </div>
                                <h4>JsonInput</h4>
                                <div>
                                    <JsonInput />
                                </div>
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Modal</span> }>
                            <div className='padded'>
                                <Modal isOpen={ isModalOpen }
                                    type={modalType}
                                    header='Modal.header'
                                    content={ <div>
                                        <h3>This is a {modalType} Modal</h3>
                                        <p>
                                        You can add any valid JSX into the Modal.content prop.
                                        There are also a type, header, footer prop.
                                        To open and close a modal, use the Modal.isOpen prop.
                                        Lastly, use the Modal.onClose prop to trigger your hiding function, when the X is pressed.
                                        </p>
                                    </div>}
                                    footer={
                                        <div className='flex-row'>
                                            <Button
                                                className='secondary full-width'
                                                onClick={() => this.setState({ isModalOpen: false })}
                                            >
                                            Cancel
                                            </Button>
                                            <Button
                                                className={`${modalType} full-width`}
                                                onClick={() => this.setState({ isModalOpen: false })}
                                            >
                                            Submit
                                            </Button>
                                        </div>
                                    }
                                    onClose={() => this.setState({ isModalOpen: false })}
                                />
                                <Button className='primary solid'
                                    onClick={() => this.setState({
                                        isModalOpen: true,
                                        modalType: 'primary'
                                    })}>
                                Show 'primary' Modal
                                </Button>
                                <Button className='secondary solid'
                                    onClick={() => this.setState({
                                        isModalOpen: true,
                                        modalType: 'secondary'
                                    })}>
                                Show 'secondary' Modal
                                </Button>
                                <Button className='success naked'
                                    onClick={() => this.setState({
                                        isModalOpen: true,
                                        modalType: 'success'
                                    })}>
                                Show 'success' Modal
                                </Button>
                                <Button className='warning naked'
                                    onClick={() => this.setState({
                                        isModalOpen: true,
                                        modalType: 'warning'
                                    })}>
                                Show 'warning' Modal
                                </Button>
                                <Button className='error naked'
                                    onClick={() => this.setState({
                                        isModalOpen: true,
                                        modalType: 'error'
                                    })}>
                                Show 'error' Modal
                                </Button>
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Progress</span> }>
                            <div className='padded'>
                                <div style={{ maxWidth: '20rem' }}>
                                    <Progress ratio={0.25} color={'red'} />
                                    <Progress ratio={0.5} color={'orange'} />
                                    <Progress ratio={0.75} color={'green'} />
                                    <Progress ratio={1} color={'#00adee'} />
                                </div>
                            </div>
                        </Accordion>
                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Switch</span> }>
                            <div className='padded'>
                                <h4>Switch on right</h4>
                                <Switch
                                    value={ switch1 }
                                    isLabelRight={ false }
                                    label={'Custom Switch 1'}
                                    onChange={ e => this.setState({ switch1: e.target.value }, () => console.log('toggle: ', JSON.stringify(e, false, 4)))}
                                />
                                <br />
                                <Switch
                                    value={ switch2 }
                                    isLabelRight={ false }
                                    label={'Custom Switch 2'}
                                    onChange={ e => this.setState({ switch1: e.target.value }, () => console.log('toggle: ', e))}
                                />
                                <br />
                                <Switch
                                    value={ switch3 }
                                    isLabelRight={ false }
                                    label={'Custom Switch 3'}
                                    onChange={ e => this.setState({ switch1: e.target.value }, () => console.log('toggle: ', e))}
                                />

                                <br />
                                <br />
                                <h4>Switch on left</h4>
                                <Switch value={ switch1 } label={'Custom Switch 1'} />
                                <br />
                                <Switch value={ switch2 } label={'Custom Switch 2'} />
                                <br />
                                <Switch value={ switch3 } label={'Custom Switch 3'} />
                            </div>
                        </Accordion>
                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Tabs</span> }>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Table</span> }>
                            <div className='padded'>
                                <Table
                                    headers={['firstName', 'surName', 'email', 'dateOfBirth']}
                                    data={[
                                        {
                                            firstName: 'Bilo',
                                            surName: 'Lwabona',
                                            email: 'bilo@vision.io'
                                        // dateOfBirth: new Date('2020/02/02')
                                        },
                                        {
                                            firstName: 'Bilo',
                                            surName: 'Lwabona',
                                            email: 'bilo@vision.io'
                                        // dateOfBirth: new Date('2020/02/02')
                                        }
                                    ]}
                                />
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Toaster</span> }>
                            <div style={{ position: 'fixed', top: '1rem', margin: 'auto' }}>
                                <Toaster toast={toast} />
                            </div>
                            <div className="flex-row">
                                <Button color='primary' onClick={() => this.addToast({
                                    type: 'primary',
                                    isDismissable: true,
                                    content: <div>
                                            This is a 'primary' toast message
                                    </div>
                                }) } >
                                        Primary
                                </Button>
                                <Button color='secondary' onClick={() => this.addToast({
                                    type: 'secondary',
                                    content: <div>
                                            This is a 'secondary' toast message
                                    </div>
                                }) } >
                                        Secondary
                                </Button>
                                <Button color='success' onClick={() => this.addToast({
                                    type: 'success',
                                    content: <div>
                                            This is a 'success' toast message
                                    </div>
                                }) } >
                                        Success
                                </Button>
                                <Button color='warning' onClick={() => this.addToast({
                                    type: 'warning',
                                    content: <div>
                                            This is a 'warning' toast message
                                    </div>
                                }) } >
                                        Warning
                                </Button>
                                <Button color='error' onClick={() => this.addToast({
                                    type: 'error',
                                    content: <div>
                                        This is an 'error' toast message
                                    </div>
                                }) } >
                                        Error
                                </Button>
                                <Button color='error' onClick={() => this.addToast({
                                    type: 'error',
                                    content: <div>
                                        This is an 'error' toast message
                                        <h1>Toaster title</h1>
                                        <p>This is a toaster with custom rendered content</p>
                                        <ul>
                                            <li>JSX</li>
                                            <li>Lists</li>
                                            <li>h1,h2,h3, etc.</li>
                                            <li>Paragraphs</li>
                                            <li>And more</li>
                                        </ul>
                                    </div>
                                }) } >
                                        Error
                                </Button>
                                <div style={{ marginBottom: '12rem' }} />
                            </div>
                        </Accordion>

                        <div className='section-end divider horizontal' />

                        <Accordion isOpenDefault title={ <span className='section'>Tree</span> }>
                            <Tree
                                data={ treeData }
                                depth={0}
                                index={0}
                                path={[]}
                                onClick={() => console.log('clickedTree') }
                            />
                        </Accordion>

                        <div className='section-end divider horizontal' />
                    </div>
                }
            />
        </div>
    }
}

export default DemoComponents
