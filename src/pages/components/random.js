const collections = [
    [
        {
            name: 'Alpha',
            text: 'Solo item collection',
            url: 'https://i.ytimg.com/vi/p7TDpx0hsn4/maxresdefault.jpg'
        }
    ],
    [
        {
            name: 'First',
            text: 'Item description (text) goes here',
            url: 'https://wallpaperaccess.com/full/38521.jpg'
        },
        {
            name: 'Second',
            text: 'This is another description for the third item',
            url: 'https://www.itl.cat/pngfile/big/293-2937024_future-city-wallpapers-city.jpg'
        },
        {
            name: 'Third',
            text: 'This is another description for the third item',
            url: 'https://wallpaperboat.com/wp-content/uploads/2019/06/future-5.jpg'
        },
        {
            name: 'Fourth',
            text: 'Tropical islands',
            url: 'https://img5.goodfon.com/wallpaper/nbig/d/51/future-gorod-liudi-zdaniia-vodoiom.jpg'
        }
    ],
    [
        {
            name: 'First',
            text: 'Item description (text) goes here',
            url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGC_Fh0pkm8eroKMNIkt9P8A1DeCIGy97gmg&usqp=CAU'
        },
        {
            name: 'Second',
            text: 'This is another description for the third item',
            url: 'https://th.bing.com/th/id/OIP.SEUVMwA6PUzgba80j2vs3wHaEK?pid=ImgDet&rs=1'
        },
        {
            name: 'Third',
            text: 'This is another description for the third item',
            url: 'https://i.ytimg.com/vi/p7TDpx0hsn4/maxresdefault.jpg'
        }
    ],
    [
        {
            name: 'Fourth',
            text: 'Tropical islands',
            url: 'https://www.7wdata.be/wp-content/uploads/2019/01/datascience3jpg.png'
        },
        {
            name: 'Fifth',
            text: 'Random Graffiti',
            url: 'https://www.theladders.com/wp-content/uploads/data2-190708.jpg'
        }
    ],
    [
        {
            name: 'First',
            text: 'Item description (text) goes here',
            url: 'https://ak.picdn.net/shutterstock/videos/1009683020/thumb/3.jpg'
        },
        {
            name: 'Second',
            text: 'Lorem to the ipsum',
            url: 'https://cdn.hipwallpaper.com/i/26/3/kSvHqF.jpg'
        },
        {
            name: 'Third',
            text: 'This is another description for the third item',
            url: 'https://wi.wallpapertip.com/wsimgs/160-1605190_computer-science-backgrounds-hd-data-science-wallpaper-hd.jpg'
        },
        {
            name: 'Fourth',
            text: 'Tropical islands',
            url: 'https://www.7wdata.be/wp-content/uploads/2019/01/datascience3jpg.png'
        },
        {
            name: 'Fifth',
            text: 'Random Graffiti',
            url: 'https://www.theladders.com/wp-content/uploads/data2-190708.jpg'
        },
        {
            name: 'Sixth',
            text: 'Another random wallpaper',
            url: 'https://i.pinimg.com/originals/af/e8/48/afe8480bff335a893da46a33275cdc10.jpg'
        }
    ]
]
export const randomCollection = () => {
    const randomIndex = Math.floor((Math.random() * collections.length))
    console.log({ randomIndex, collection: collections[randomIndex] })
    return collections[randomIndex]
}
