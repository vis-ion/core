import { randomCollection } from './random'

export const dropdownOptionsDefault = [
    {
        label: 'One',
        value: 1
    },
    {
        label: 'Two',
        value: 2
    },
    {
        label: 'Three',
        value: 3
    },
    {
        label: 'Four',
        value: 4
    }
]

export const collections = [
    {
        name: 'My first collection',
        text: 'This is a test collection. Click it to see what happens',
        items: randomCollection()
    },
    {
        name: 'Another collection',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    },
    {
        name: 'Third collection',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    },
    {
        name: 'Fourth collection',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    },
    {
        name: 'Fifth one',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    },
    {
        name: 'Sixth collection',
        text: 'This is a test collection. Click it to see what happens',
        items: randomCollection()
    },
    {
        name: 'Lucky number Seven',
        text: 'This is a test collection. Click it to see what happens',
        items: randomCollection()
    },
    {
        name: 'Hateful Eight',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    },
    {
        name: 'Nein nein nein',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    },
    {
        name: 'Top Ten',
        text: 'Click on this CardStack to see how it expands to reveal data',
        items: randomCollection()
    }
]

export const treeData = {
    title: 'My Data Tree',
    children: [
        {
            title: 'Group 1',
            children: [
                {
                    title: 'Nested Group 1',
                    children: [
                        {
                            title: 'nested 1',
                            status: 'success'
                        }, {
                            title: 'Node with really long title, to test the text wrapping capabilities of the tree ' +
                                    'components',
                            status: 'normal'
                        }, {
                            title: 'nested 4',
                            status: 'loading'
                        }
                    ]
                }, {
                    title: 'Nested 2: Icons',
                    children: [
                        {
                            title: 'success',
                            status: 'success'
                        }, {
                            title: 'warning',
                            status: 'warning'
                        }, {
                            title: 'error',
                            status: 'error'
                        }, {
                            title: 'loading',
                            status: 'loading'
                        }, {
                            title: 'checkbox',
                            status: 'checkbox'
                        }
                    ]
                }, {
                    title: 'Data Leaf',
                    status: 'checkbox'
                }, {
                    title: 'Nested Group 3',
                    children: [
                        {
                            title: 'Nested 3.3',
                            children: [
                                {
                                    title: 'Nested 3.3',
                                    children: [
                                        {
                                            title: 'nested 1',
                                            status: 'success'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }, {
            title: 'Data Leaf'
        }, {
            title: 'More data'
        }, {
            title: 'And More '
        }, {
            title: 'More Data for everyone'
        }, {
            title: 'Hello Data'
        }
    ]
}
