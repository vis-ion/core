import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import {
    Accordion,
    DocIndex,
    Layout
} from 'components'

import AppMaster from 'apps/master'

export class DemoApps extends Component {
    state = {
    }

    setChartType = e => {
        this.setState({
            chartType: e.target.value
        })
    }

    componentDidMount () {
    }

    onChange = e => {
    }

    render () {
        return <div className='page'>
            <div id='demo' className='page'>
                <h2 className='padded'>Apps</h2>
                {/**
                        <Accordion title={<span className='section'>Calendar</span>}>
                        </Accordion>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <Accordion title={<span className='section'>Canvas</span>}>
                        </Accordion>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <Accordion title={<span className='section'>Clock</span>}>
                        </Accordion>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <Accordion title={<span className='section'>counter</span>}>
                        </Accordion>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <Accordion title={<span className='section'>Currency</span>}>
                        </Accordion>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                    */}
                <Accordion title={<span className='section'>Instagram</span>}>
                    <AppMaster
                        isEditable={ true }
                        type='app:instagram'
                        data={{
                            hashtag: 'hdr'
                        }}
                    />
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Google</span>}>
                    <AppMaster
                        isEditable={ true }
                        type='app:googleSlides'
                        data={{
                            url: 'https://docs.google.com/presentation/d/1o8hY192y7Eltj66MXeguZG9VTWpruQtCo5G8BUE2Dro/edit?usp=sharing'
                        }}
                    />
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Map</span>}>
                </Accordion>
                {/* <AppMaster
                        type='app:map'
                        data={{

                        }}
                    /> */}
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>News</span>}>
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Pinterest</span>}>
                    {/* <AppMaster
                            isEditable={ true }
                            type='app:pinterest'
                            data={{
                                url: ''
                            }}
                        /> */}
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Spotify</span>}>
                    <AppMaster
                        isEditable={ true }
                        type='app:spotify'
                        data={{
                            url: ''
                        }}
                    />
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>SoundCloud</span>}>
                    <AppMaster
                        isEditable={ true }
                        type='app:soundcloud'
                        data={{
                            url: 'https://soundcloud.com/celldweller/shutemdown',
                            trackId: 55106914
                        }}
                    />
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Vimeo</span>}>
                    <AppMaster
                        isEditable={ true }
                        type='app:vimeo'
                        data={{
                            url: 'https://vimeo.com/167962413'
                        }}
                    />
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Weather</span>}>
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
                <Accordion title={<span className='section'>Youtube</span>}>
                    <AppMaster
                        isEditable={ true }
                        type='app:youtube'
                        data={{
                            url: 'https://www.youtube.com/watch?v=RENHM-6Dxs4'
                        }}
                    />
                </Accordion>
                <div className='section-end' />
                <div className='divider horizontal' />
            </div>
        </div>
    }
}

export default DemoApps
