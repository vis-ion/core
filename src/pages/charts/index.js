import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import {
    DocIndex,
    Markdown,
    Layout,
    Accordion,
    Card,
    Button,
    Input,
    CheckBox,
    Select,
    Switch,
    Toaster
} from 'components'

export class DemoCharts extends Component {
    state = {
    }
    setChartType = e => {
        this.setState({
            chartType: e.target.value
        })
    }
    componentDidMount() {
    }
    onChange = e => {
    }
    render () {
        return <div className='page'>
            <Layout
                leftDiv={
                    <DocIndex
                        headerQuery={'#demo>.section'}
                        footerQuery={'#demo>.section-end'}
                    />
                }
                rightDiv={
                    <div id='demo' className='page'>
                        <h2>Charts</h2>
                        <h3 className='section'>Bar Chart</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <h3 className='section'>Pie Chart</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <h3 className='section'>Line Chart</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <h3 className='section'>Area Chart</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <h3 className='section'>Sunburst Chart</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />
                        <h3 className='section'>Radar Chart</h3>
                        <div className='section-end' />
                    </div>
                }
            />
        </div>
    }
}

export default DemoCharts
