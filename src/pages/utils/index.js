import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import {
    DocIndex,
    Markdown,
    Layout,
    Accordion,
    Card,
    Button,
    Input,
    CheckBox,
    Select,
    Switch,
    Toaster
} from 'components'

export class DemoUtils extends Component {
    state = {
    }
    setChartType = e => {
        this.setState({
            chartType: e.target.value
        })
    }
    componentDidMount() {
    }
    onChange = e => {
    }
    render () {
        return <div className='page'>
            <Layout
                leftDiv={
                    <DocIndex
                        headerQuery={'#demo>.section'}
                        footerQuery={'#demo>.section-end'}
                    />
                }
                rightDiv={
                    <div id='demo' className='page'>
                        <h2>Utils</h2>

                        <h3 className='section'>Currency </h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />

                        <h3 className='section'>Geographic</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />

                        <h3 className='section'>URLs</h3>
                        <div className='section-end' />
                        <div className='divider horizontal' />

                    </div>
                }
            />
        </div>
    }
}

export default DemoUtils
