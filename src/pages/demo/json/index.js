import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import { Code, Markdown, Layout, JsonInput, Tabs } from 'components'
import data from '../../../demo/mock-data/1d'
import options from './config'
import { MasterChart } from 'charts'

export class JsonDemo extends Component {
    state = {
        jsonString: '',
        jsonObject: data,
        chartType: 'chart:pie'
    }
    setChartType = e => {
        this.setState({
            chartType: e.target.value
        })
    }
    componentDidMount() {
        this.setState({
            jsonObject: data,
            jsonString: JSON.stringify(data, false, 2)
        })
    }
    onChange = e => {
        try {
            this.setState({
                jsonString: e.target.value,
            })
            // }, () => this.setState({ jsonObject: JSON.parse(e.target.value) }))
        } catch (e) {
            this.setState({
                error: e
            })
        }
    }
    render () {
        const { jsonString, jsonObject, chartType } = this.state

        const chart = <MasterChart
            type={ chartType }
            data={ jsonObject }
        />

        return <div className='page'>
            <Layout
                rightDiv={ chart }
                leftDiv={ <Tabs
                    defaultTab='config'
                    keys={['config', 'code','json']}
                    code={ <Code code={ chart } /> }
                    config={
                        <div>
                            <div className='page-header padded'>
                                <FAIcon className='icon' name={ options.icon } />
                                <br />
                                <div className='title'>{ options.name }</div>
                            </div>
                            <div className='divider horizontal'/>
                            <label>chart-type</label>
                            <br />
                            <input value={ chartType } onChange={ this.setChartType } />
                        </div>
                    }
                    json={
                        <div>
                            <label>chart-data</label>
                            <JsonInput
                                value={ jsonString }
                                onChange={ this.onChange }
                            />
                        </div>
                    }
                />
                }
            />
        </div>
    }
}

export default JsonDemo
