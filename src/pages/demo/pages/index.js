import React, { Component } from 'react'
import { Code, ConfigField, Tabs, Layout, Markdown, JsonInput } from 'components'
import { AppMaster } from 'apps'
import FAIcon from 'react-fontawesome'

export class PagesDemo extends Component {
    state = {
        config: {
            // ...config,
            // props: config.props.map(field => ({ ...field, value: field.defaultValue }))
        },
        values: {}
    }
    onChange = (key) => e => {
        const { value } = e.target
        this.setState({
            values: {
                ...this.state.values,
                [key]: value
            }
        }, () => console.log(this.state.values))
    }
    render () {
        const { jsonString, jsonObject, config } = this.state

        return <div className='page'>
            <Layout
                rightDiv={ <div /> }
                leftDiv={ <div></div> }
            />
        </div>
    }
}

export default AppsDemo
