import palette from 'util/palette'
export default {
    name: 'Grid Chart',
    type: 'chart:grid',
    icon: 'th',
    props: [
        {
            name: 'palette',
            type: 'palette',
            defaultValue: palette
        },
        {
            name: 'verticalCompact',
            type: 'boolean',
            defaultValue: true,
        },
        {
            name: 'rowHeight',
            type: 'number',
            defaultValue: 6,
        },
        {
            name: 'columns',
            type: 'number',
            defaultValue: 12,
        }
    ]
}