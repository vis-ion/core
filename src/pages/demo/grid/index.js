import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import { Accordion, Code, ConfigField, Tabs, Layout, Markdown, JsonInput } from 'components'
import options from './config'
import data from 'demo/mock-data/1d'
import { ChartsGrid } from 'components/grid'

export class GridDemo extends Component {
    state = {
        options: {
            ...options,
            props: options.props.map(field => ({ ...field, value: field.defaultValue }))
        },
        values: {}
    }
    onChange = (key) => e => {
        const { value } = e.target
        this.setState({
            values: {
                ...this.state.values,
                [key]: value
            }
        }, () => console.log(this.state.values))
    }
    render () {
        const { jsonString, jsonObject } = this.state
        const chart = <ChartsGrid
            isExportable
            data={data}
            items={[
                {
                    layout: { x: 0, y: 0, w: 3, h: 4 },
                    type: 'chart:pie',
                    options: {
                        width: 200,
                        height: 200
                    }
                },
                {
                    layout: { x: 3, y: 0, w: 4, h: 4 },
                    type: 'chart:bar',
                    options: {
                        width: 300,
                        height: 200
                    }
                },
                {
                    layout: { x: 7, y: 0, w: 4, h: 4 },
                    type: 'chart:line',
                    options: {
                        width: 300,
                        height: 200
                    }
                },
                // {
                //     layout: { x: 7, y: 4, w: 4, h: 4 },
                //     type: 'chart:table',
                //     options: {
                //         width: 300,
                //         height: 200
                //     }
                // }
            ]}
        />
        return <div className='page'>
            <Layout
                rightDiv={ chart }
                leftDiv={
                    <Tabs
                        defaultTab='options'
                        keys={['options', 'code']}
                        code={ <Code code={ chart } /> }
                        options={
                            <div>
                                <div className='page-header padded'>
                                    <FAIcon className='icon' name={ options.icon } />
                                    <br />
                                    <div className='title'>{ options.name }</div>
                                </div>
                                <div className='divider horizontal'/>
                                {
                                    options.props.map((prop, i) => <div key={ i }>
                                        <ConfigField
                                            onChange={ this.onChange }
                                            type={ prop.type }
                                            label={ prop.name }
                                            name={ prop.name }
                                            defaultValue={ prop.defaultValue }
                                        />
                                    </div>)
                                }
                            </div>
                        }
                    />
                }
            />
        </div>
    }
}

export default GridDemo
