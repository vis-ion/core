import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import { Code, ConfigField, Tabs, Layout, Markdown, JsonInput } from 'components'
import options from 'charts/bar/config'
import data from 'demo/mock-data/1d'
import { BarChart } from 'charts'

export class BarDemo extends Component {
    state = {
        options: {
            ...options,
            props: options.props.map(field => ({ ...field, value: field.defaultValue }))
        },
        values: {}
    }
    componentDidMount() {
        this.setState({
            jsonObject: data,
            jsonString: JSON.stringify(data, false, 2)
        })
    }
    onChange = (key) => e => {
        const { value } = e.target
        this.setState({
            values: {
                ...this.state.values,
                [key]: value
            }
        }, () => console.log(this.state.values))
    }
    render () {
        const { jsonString, jsonObject } = this.state

        const chart = <BarChart
            options={{
                ...options,
                width: 100,
                height: 50,
                range: [0, 100]
            }}
            data={ data }
        />

        return <div className='page'>
                <Layout
                    rightDiv={ chart }
                    leftDiv={ <Tabs
                        defaultTab='options'
                        keys={['options', 'code','json']}
                        code={ <Code
                                isEditable
                                code={ chart }
                            /> }
                        options={
                            <div>
                                <div className='page-header padded'>
                                    <FAIcon className='icon' name={ options.icon } />
                                    <br />
                                    <div className='title'>{ options.name }</div>
                                </div>
                                <div className='divider horizontal'/>
                                {
                                    options.props.map((prop, i) => <div key={ i }>
                                        <ConfigField
                                            onChange={ this.onChange }
                                            type={ prop.type }
                                            label={ prop.name }
                                            name={ prop.name }
                                            defaultValue={ prop.defaultValue }
                                        />
                                    </div>)
                                }
                            </div>
                        }
                        json={
                            <div>
                                <label>chartData</label>
                                {/* <JsonInput
                                    isEditable
                                    value={ jsonString }
                                    onChange={ this.onChange }
                                /> */}
                                <Code
                                    isEditable
                                    code={{  string: jsonString }}
                                    language='json'
                                />
                            </div>
                        }
                    />
                    }
                />
        </div>
    }
}

export default BarDemo
