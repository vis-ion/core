import React, { Component } from 'react'
import { Code, ConfigField, Tabs, Layout, Markdown, JsonInput } from 'components'
import { AppMaster } from 'apps'
import FAIcon from 'react-fontawesome'

export class AppsDemo extends Component {
    state = {
        config: {
            // ...config,
            // props: config.props.map(field => ({ ...field, value: field.defaultValue }))
        },
        values: {}
    }
    onChange = (key) => e => {
        const { value } = e.target
        this.setState({
            values: {
                ...this.state.values,
                [key]: value
            }
        }, () => console.log(this.state.values))
    }
    render () {
        const { jsonString, jsonObject, config } = this.state
        const widgets = <div>
            <AppMaster
                type='app:text'
                data={{
                    content: 'This is just a text widget'
                }}
                options={{
                    color: 'red',
                    fontSize: '16px'
                }}
            />
            <AppMaster
                type='app:media:image'
            />
            <AppMaster
                type='app:audio'
            />
            <AppMaster
                type='app:video'
                options={{
                    width: 480,
                    height: 320
                }}
            />
        </div>
        return <div className='page'>
            <Layout
                rightDiv={ widgets }
                leftDiv={
                    <Tabs
                        defaultTab='config'
                        keys={['config', 'code']}
                        code={ <Code code={ widgets } /> }
                        config={
                            <div>
                                <h3>{ config.name }</h3>
                                {
                                    (config.props || []).map((prop, i) => <div key={ i }>
                                        <ConfigField
                                            onChange={ this.onChange }
                                            type={ prop.type }
                                            label={ prop.name }
                                            name={ prop.name }
                                            defaultValue={ prop.defaultValue }
                                        />
                                    </div>)
                                }
                            </div>
                        }
                    />
                }
            />
        </div>
    }
}

export default AppsDemo
