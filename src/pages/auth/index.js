import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import {
    DocIndex,
    Layout,
    Accordion,
    Card,
    Button,
    Input,
    CheckBox,
    Select,
    Switch,
    Toaster,
} from 'components'

import {
    AuthForm
} from 'auth/form'

const authProviders = [
    'google',
    'facebook'
]

export class DemoPages extends Component {
    state = {
        login: {},
        register: {},
        resetPassword1: {},
        resetPassword2: {},
        authCode: {},
    }
    componentDidMount() {
    }
    onChange = e => {
    }
    updateForm = (form) => (obj) => {
        this.setState({
            ...this.state,
            [form]: {
                ...this.state[form],
                ...obj
            }
        })
    }
    submitForm = (form) => () => {
        const stateForm = this.state[form]
        console.log({ [form]: stateForm })
        alert(form, JSON.stringify(stateForm))
    }
    render () {
        const {
            login,
            register,
            resetPassword1,
            resetPassword2,
            authCode
        } = this.state
        return (
        <div className='page'>
            <Accordion isOpenDefault title={ 'Login'}>
                {/* Login */}
                <AuthForm
                    title={'Login'}
                    onChange={this.updateForm('login')}
                    onSubmit={this.submitForm('login')}
                    providers={authProviders}
                    fields={[
                        'email',
                        'password'
                    ].map(key => ({ key, type: key, label: key }))}
                    submitText={'Log in'}
                />
            </Accordion>
            {/* Register */}
            <Accordion isOpenDefault title={ 'Register'}>
                <AuthForm
                    title={'Register'}
                    submitText={'Register'}
                    onChange={this.updateForm('register')}
                    onSubmit={this.submitForm('register')}
                    fields={
                        [
                            { key: 'name', type: 'text', label: 'Name' },
                            { key: 'surname', type: 'text', label: 'Surname' },
                            { key: 'city', type: 'text', label: 'City' }
                        ]
                    }
                />
            </Accordion>
            {/* Reset Password */}
            <Accordion isOpenDefault title={ 'Reset Password'}>
                <AuthForm
                    title={'Reset Password'}
                    onChange={this.updateForm('resetPassword1')}
                    onSubmit={this.submitForm('resetPassword1')}
                    submitText={'Send reset email'}
                    fields={[
                        { key: 'email', type: 'email', label: 'Email' },
                    ]}
                />
            </Accordion>
            <Accordion isOpenDefault title={ 'Create New Password'}>
                <AuthForm
                    title={'Create New Password'}
                    onChange={this.updateForm('resetPassword2')}
                    onSubmit={this.submitForm('resetPassword2')}
                    submitText={'Change password'}
                    fields={[
                        { key: 'newPassword1', type: 'text', label: 'New Password' },
                        { key: 'newPassword2', type: 'text', label: 'Confirm Password' }
                    ]}
                />
            </Accordion>
            {/* Auth Code */}
            <Accordion isOpenDefault title={'Auth Code'}>
                <AuthForm
                    title={'Auth Code'}
                    onChange={this.updateForm('authCode')}
                    onSubmit={this.submitForm('authCode')}
                    fields={[
                        { key: 'code', type: 'number', label: 'Auth Code' },
                    ]}
                    submitText={'Continue'}
                />
            </Accordion>
        </div>
        )
    }
}

export default DemoPages
