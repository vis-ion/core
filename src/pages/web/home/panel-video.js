import React from 'react'
import { AppVideo } from 'apps'
export const PanelVideo = ({ children, src }) => <div style={{ position: 'relative' }}>
    <AppVideo
        style={{
            position: 'absolute',
            right: '0',
            bottom: '0',
            minWidth: '100%',
            minHeight: '480px'
        }}
        data={{
            src
        }}
        options={{
            width: '100%',
            height: '100%'
        }}
    />
    <div className='video-panel'>
        {
            children
        }
    </div>
</div>

export default PanelVideo
