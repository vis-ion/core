import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { Accordion, Code, AppVideo } from 'components'
import { PanelImage } from './panel-image'
import { PanelVideo } from './panel-video'
import data from 'demo/mock-data/1d'
import logo from 'img/vision-logo-2-blue.png'
import media from 'img/vision_mac_media_design.png'
import appStore from 'img/vision_app_store.png'
import matter from 'img/vision_shapes_matter.png'
import imgIntegration from 'img/vision_integration.png'

import vidApps from 'img/home/vision_apps.mp4'
import vidBanner from 'img/home/vision_banner.mp4'
import vidMatter from 'img/home/vision_matter.mp4'
import vidIntegration from 'img/home/vision_integration.mp4'
import vidSpacetime from 'img/home/vision_spacetime.mp4'
import vidTube from 'img/home/vision_tube.mp4'
import vidLightBlue from 'img/video/light-blue.mp4'
import vidLightGreen from 'img/video/light-green.mp4'
// import { MasterChart } from 'components/charts'
// import { MasterGrid } from 'components/grid'
const nodePackage = require('../../../../package.json')

export class Home extends Component {
    goTo = path => () => {
        this.props.history.push(path)
    }

    render () {
        return <div className='page flex-col space-between'>
            {/* VISION */}
            <PanelVideo src={ vidLightGreen }>
                <img src={ logo }/>
                <div className='title'>vision</div>
                <br />
                <div>visualise all your data in ways never before imagined</div>
                <br />
                <button className='primary hollow' onClick={this.goTo('/')}>Get Started</button>
            </PanelVideo>
            <PanelImage color='black' style={{ position: 'absolute', minHeight: '10rem' }}>
                <h2>again</h2>
                <p>vision is a digital visualisation and sharing platform</p>
                <img src={ media } style={{
                    width: '35rem',
                    height: 'auto',
                    marginBottom: '1rem'
                }} />
                <div>in other words, we basically provide digital signage solutions</div>
            </PanelImage>
        </div>
    }
}

export default withRouter(Home)
