import React, { Component } from 'react'
import { withRouter } from 'react-router'

export class NotFound extends Component {
    render () {
        return <div className='page'>
            <h1>NotFound</h1>
            <button onClick={ () => this.props.history.push('/core')}>Go Home</button>
        </div>
    }
}

export default withRouter(NotFound)
