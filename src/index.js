import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import './styles/index.scss'
import './favicon.ico'
import pkg from '../package.json'
console.clear()
console.log(`${pkg.name}: v${pkg.version}`)

ReactDOM.render(
    <App />
, document.getElementById('root'))
