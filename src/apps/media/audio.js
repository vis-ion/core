import React, { Component } from 'react'

export class AppAudio extends Component {
    static defaultProps = {
        options: {
            width: 320,
            height: 240
        },
        data: {
            src: 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3',
            format: 'mpeg' // mp3
        }
    }
    render () {
        const { data, options } = this.props
        const { width, height } = options
        const { src, format } = data
        // TODO: https://wavesurfer-js.org/
        return <audio width={ width } height={ height } controls>
                <source src={ src } type={ `audio/${format || 'mpeg'}` } />
                Your browser does not support the audio tag: '{format}'
            </audio>
    }
}

AppAudio.formats = [
    {
        name: 'MP3',
        format: 'audio/mpeg'
    },
    {
        name: 'OGG',
        format: 'audio/ogg'
    },
    {
        name: 'WAV',
        format: 'audio/wav'
    }
]

AppAudio.options = {
    name: 'AppAudio',
    type: 'app:audio',
    icon: 'music',
    props: [{
        name: 'url',
        type: 'text',
        defaultValue: '',
    }]
}

export default AppAudio
