import React, { Component } from 'react'

export class AppVideo extends Component {
    static defaultProps = {
        options: {
            width: 320,
            height: 240,
            autoplay: true,
        },
        data: {
            src: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4',
            format: 'mp4'
        }
    }
    render () {
        const { data, options, style } = this.props
        const { autoplay, width, height } = options
        const { src, format } = data
        return <video
            // controls 
            autoPlay
            loop
            width={ width }
            height={ height }
            style={{ ...style, backgroundColor: 'black'}}>
            <source src={ src } type={ `video/${format || 'mp4'}` } />
            Your browser does not support the video tag: '{format}'
        </video>
    }
}

AppVideo.formats = [
    {
        name: 'MP4',
        format: 'video/mp4'
    },
    {
        name: 'WebM',
        format: 'video/webm'
    },
    {
        name: 'Ogg',
        format: 'video/ogg'
    }
]

AppVideo.options = {
    name: 'AppVideo',
    type: 'app:video',
    icon: 'video',
    props: [{
        name: 'url',
        type: 'text',
        defaultValue: '',
    }]
}

export default AppVideo
