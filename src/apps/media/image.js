import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input } from 'components'
// import { TextField } from '@material-ui/core'

export class AppImage extends Component {
    static propTypes = {
        data: PropTypes.object,
        duration: PropTypes.func,
        onDataChange: PropTypes.func,
    }
    static defaultProps = {
        onDataChange: (data) => console.warn('define <AppImage />.onDataChange()'),
        duration: 3
    }
    state = {
        url: ''
    }
    componentDidMount() {
        const { duration, onNext, data } = this.props
        setTimeout( onNext, duration * 1000)
        this.setState({
            url: data.url
        })
    }
    onChange = (e) => {
        const url = e.target.value
        this.setState({
            url
        })
        this.props.onDataChange({ url })
    }
    render () {
        const { data, isEditable } = this.props
        const { url } = this.state
        return <div className='flex-col'>
            {
                isEditable && <Input
                    label='Type something...'
                    type='text'
                    value={ url }
                    onChange={this.onChange}
                />
            }
            <br />
            <img src={ url } />
        </div>
    }
}

export default AppImage
