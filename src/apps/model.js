import React, { Component } from 'react'

export class ModelApp extends Component {
    static defaultProps = {
        options: {
            width: 320,
            height: 240
        },
        data: {
            key: 'modelKey',
            model: (data) => data 
        }
    }

    onChange = () => {

    }
    render () {
        const { data, options, onChange } = this.props
        const { width, height } = options
        const { keys } = data

        return <div>
            { keys.map((key, i) => <div><label>{key}<input type='checkbox' /></label></div>)}
        </div>
    }
}

ModelApp.options = {
    name: 'ModelApp',
    type: 'app:filter',
    icon: 'filter',
    props: [{
        name: 'url',
        type: 'text',
        defaultValue: '',
    }]
}

export default ModelApp
