import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'components'
import Map from 'components/map'

export class AppMap extends Component {
    static propTypes = {
        // data: PropTypes.object.isRequired,
        isEditable: PropTypes.bool,
        duration: PropTypes.number,
        onNext: PropTypes.func,
    }
    render () {
        const { data } = this.props
        const { points } = data || {points: []}
        return <div>
            <h1>App</h1>
            <Card className='map-view'>
                <Map layerStyle={'light'} points={points || []} />
            </Card>
        </div>
    }
}

export default AppMap
