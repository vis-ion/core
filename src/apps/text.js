import React, { Component } from 'react'

export class TextApp extends Component {
    render () {
        const { data, options } = this.props
        const style = options && options.style
            ? options.style
            : {
                color: '#00adee',
                fontWeight: 'bold',
                fontSize: '1.5rem'
            }
        return data && data.content
            ? <div style={ style }>{data.content}</div>
            : <div>TextApp</div>
    }
}

TextApp.options = {
    name: 'TextApp',
    type: 'app:text',
    icon: 'font',
    props: [{
        name: 'text',
        type: 'text',
        defaultValue: '',
    }]
}

export default TextApp
