import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Iframe, Input } from 'components'
// import { TextField } from '@material-ui/core'

'https://player.vimeo.com/video/239029778?byline=0&portrait=0'
'https://vimeo.com/channels/music/239029778'


export class AppVimeo extends Component {
    static defaultProps = {
        onDataChange: (data) => console.log('define <AppImage />.onDataChange()'),
        isAutoplay: true,
        isLooping: false,
        duration: 4
    }
    state = {
        url: '',
        embedUrl: ''
    }
    componentDidMount () {
        const { duration, onNext, url } = this.props
        this.generateEmbedUrl()
        setTimeout(onNext, duration * 1000)
    }
    generateEmbedUrl = () => {
        const { isAutoplay, isLooping, data } = this.props
        const videoId = data.url.split('/').pop()
        this.setState({
            url: data.url,
            embedUrl: `https://player.vimeo.com/video/${videoId}?byline=0&portrait=0&autoplay=${Number(!!isAutoplay)}&loop=${!!isLooping}&autopause=0`
        }, () => this.props.onDataChange({
            url: this.state.url,
            embedUrl: this.state.embedUrl
        }))

    }
    render () {
        const { embedUrl, isEditable } = this.state
        return <div className='flex-col'>
            {
                isEditable && <Input
                    label='URL'
                    type='text'
                    value={query}
                    onChange={this.getEmbedUrl}
                />
            }
            <br />
            {
                embedUrl && <div className='iframe-container'>
                <iframe
                    src={embedUrl}
                    frameBorder='0'
                    width={960}
                    height={960}
                    allowFullScreen='true'
                    mozallowfullscreen='true'
                    webkitallowfullscreen='true'
                />
            </div>
            }
        </div>
    }
}

export default AppVimeo