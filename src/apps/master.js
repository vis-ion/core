import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card, Icon } from '../components'
import {
    AppCalendar,
    AppClock,
    AppCountdown,
    AppEmbed,
    AppGoogle,
    AppImage,
    AppInstagram,
    AppMap,
    AppNews,
    AppSoundcloud,
    AppSpotify,
    AppText,
    AppVimeo,
    AppWeather,
    AppYoutube,
} from './'

export class AppMaster extends Component {
    static propTypes = {
        type: PropTypes.string.isRequired,
        data: PropTypes.object.isRequired
    }

    static defaultProps = {
        onChange: (data) => {}
    }

    render () {
        const {
            isEditable,
            onDelete,
            onChange,
            data,
            type,
        } = this.props

        const props = {
            data,
            isEditable,
            onDelete,
            onChange
        }

        switch (type) {
            // case 'app:calendar':
                // return <AppCalendar { ...props } />
            // case 'app:media:image':
            //     return <AppImage { ...props } />
            // case 'app:text':
            //     return <AppText { ...props } />
            // case 'app:countdown':
            //     return <AppCountdown { ...props } />
            // case 'app:clock':
            //     return <AppClock { ...props } />
            // case 'app:worldClock':
            //     return <AppWorldClock { ...props } />
            case 'app:soundcloud':
                return <AppSoundcloud { ...props } />
            case 'app:spotify':
                return <AppSpotify { ...props } />
            case 'app:youtube':
                return <AppYoutube { ...props } />
            // // TODO: add google search widget
            // case 'app:google:docs':
            // case 'app:googleDocs':
            //     return <AppGoogle type='docs' { ...props } />
            // case 'app:google:sheets':
            // case 'app:googleSheets':
            //     return <AppGoogle type='sheets' { ...props } />
            // case 'app:google:slides':
            // case 'app:googleSlides':
            //     return <AppGoogle type='slides' { ...props } />
            case 'app:instagram':
                return <AppInstagram { ...props } />
            case 'app:vimeo':
                return <AppVimeo { ...props } />
            case 'app:news':
                return <AppNews { ...props } />
            case 'app:weather':
                return <AppWeather { ...props } />
            case 'app:embed':
                return <AppEmbed { ...props } />
            // case 'app:map':
            //     return <AppMap { ...props } />
            default:
                return <div style={{
                    padding: '1rem',
                    width: 'calc(100% - 2rem)'
                }}>
                    <h2>
                        {/* <Icon name='exclamation-circle' /> */}
                        &nbsp;&nbsp;&nbsp;
                        Unsupported Widget Type</h2>
                    <p style={{ fontSize: '1.2rem' }}>
                        Widgets of type &nbsp;&nbsp;<code><strong>{type}</strong></code>&nbsp;&nbsp; are not currently supported.
                    </p>
                </div>
        }
    }
}

export default AppMaster