import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class AppWorldClock extends Component {
    static propTypes = {
        // data: PropTypes.object.isRequired,
        isEditable: PropTypes.bool,
        duration: PropTypes.number,
        onNext: PropTypes.func,
    }
    static defaultProps = {

    }
    state = {

    }
    render () {
        return <div>
            <h1>App: WorldClock</h1>
        </div>
    }
}

export default AppWorldClock