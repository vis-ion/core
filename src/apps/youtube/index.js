import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Iframe, Input } from 'components'
// import { TextField } from '@material-ui/core'

export class AppYoutube extends Component {
    static propTypes = {
        onDataChange: PropTypes.func,
    }
    static defaultProps = {
        isAutoplay: true,
        isLooping: false,
        onDataChange: (data) => console.log('define <AppYoutube/>.onDataChange()'),
        duration: 4
    }
    state = {
        url: '',
        videoId: undefined
    }
    componentDidMount() {
        console.log('data:', this.props.data)
        const { duration, onNext } = this.props
        setTimeout(onNext, duration * 1000)
        this.getVideoId(this.props.data.url)
        this.setState({
            url: this.props.data.url,
        })
    }
    getVideoId = (url) => {
        const urlParts = url.split('?v=')
        const videoId = urlParts[urlParts.length - 1]
        const data = { url, videoId }
        console.log(`getVideoId: `, data)
        this.setState({ ...data })
        this.props.onDataChange(data)
    }
    render () {
        const { onChange, data, isEditable } = this.props
        const { url, videoId } = this.state

        return <div>
            {
                isEditable && <Input
                label='URL'
                type='text'
                style={{
                    width: '100%'
                }}
                value={url}
                onChange={(e) => this.getVideoId(e.target.value)}
            />
            }
            <br />
            {
                url && <div className='iframe-container'>
                    <iframe
                        src={`https://www.youtube.com/embed/${videoId}`}
                        frameBorder='0'
                        width={960}
                        height={960}
                        allowFullScreen='true'
                        mozallowfullscreen='true'
                        webkitallowfullscreen='true'
                        >{data.url}
                    </iframe>
                </div>
            }
        </div>
    }
}

export default AppYoutube
