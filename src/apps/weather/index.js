import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { getCurrentWeather, getWeatherForecast } from './util'
import './style.scss'

export class AppWeather extends Component {
    static propTypes = {
        // data: PropTypes.object.isRequired,
        isEditable: PropTypes.bool,
        duration: PropTypes.number,
        onNext: PropTypes.func,
    }
    static defaultProps = {
        duration: 1
    }

    state = {
        weather: {
            alt_ft: 137.8,
            alt_m: 42,
            cloudtotal_pct: 50,
            dewpoint_c: 12.98,
            dewpoint_f: 55.36,
            feelslike_c: 18.9,
            feelslike_f: 66.02,
            humid_pct: 68,
            lat: -33.98,
            lon: 18.6,
            slp_in: 29.97,
            slp_mb: 1011.9,
            temp_c: 19,
            temp_f: 66.2,
            vis_desc: null,
            vis_km: 10,
            vis_mi: 6.21,
            winddir_compass: "SW",
            winddir_deg: 220,
            windspd_kmh: 17,
            windspd_kts: 9.18,
            windspd_mph: 10.56,
            windspd_ms: 4.72,
            wx_code: 1,
            wx_desc: 'Partly cloudy',
            wx_icon: 'PartlyCloudyNight.gif'
        }
    }
    componentDidMount() {
        const { duration, onNext } = this.props
        setTimeout(onNext, duration * 1000)

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                const { coords } = position
                // console.log(coords.latitude, coords.longitude)
                // getCurrentWeather(coords).then(response => {
                //     console.log('weather: ', response.data)
                // })
            })
        }
    }
    render () {
        const { weather } = this.state
        return <div className='weather-container'>
            <div className='summary'>
                <span className='temperature'>{weather.temp_c} ºC</span>
                <span className='weather-icon'>{}</span>
            </div>
            <div className='wind'>
                <span className='arrow'></span>
                <span className='speed'></span>
            </div>
        </div>
    }
}

export default AppWeather