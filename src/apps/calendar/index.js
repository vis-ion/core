import React, { Component } from 'react'
import { Card } from 'components'
import PropTypes from 'prop-types'
import './style.scss'

let timeUpdateId
export class AppCalendar extends Component {
    static defaultProps = {
        duration: 2
    }
    state = {
        date: new Date(),
        months: [
            { name: 'January', days: 31},
            // TODO: check leap years
            { name: 'February', days: 28},
            { name: 'March', days: 31},
            { name: 'April', days: 30},
            { name: 'May', days: 31},
            { name: 'June', days: 30},
            { name: 'July', days: 31},
            { name: 'August', days: 31},
            { name: 'September', days: 30},
            { name: 'October', days: 31},
            { name: 'November', days: 30},
            { name: 'December', days: 31}
        ],
        daysOfWeek: [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ]
    }
    componentDidMount () {
        const { duration, onNext } = this.props
        setTimeout(onNext, duration * 1000)

        timeUpdateId = setInterval(() => {
            this.setState({
                date: new Date()
            })
        }, 60000) // NOTE: updates date once a minute
    }
    componentWillUnmount() {
        clearInterval(timeUpdateId)
    }
    render() {
        const { date, months, daysOfWeek } = this.state
        const getDayOfMonth = (date) => {
            return <Card className={'day-card'} depth='4'>
                <div className='week-day'>
                    {
                        daysOfWeek[date.getUTCDay()]
                    }
                </div>
                <br />
                <div className='month-day'>
                    {
                        date.getDate()
                    }
                </div>
            </Card>
        }
        const getMonth = (date) => {
            return <Card className={'month-card'} depth='4'>
                <div className='month-name'>
                    {
                        months[date.getMonth()].name
                    }
                </div>
                <div className='calendar'>
                    {
                        [ ...new Array(months[date.getMonth()]
                            .days)]
                            .map( (day, i) => <div
                                    key={`month-day-${i+1}`}
                                    className='day'>
                                        {i+1}
                                </div>
                            )
                    }
                </div>
            </Card>
        }
        return <div className='calendar-container'>
            {getDayOfMonth(date)}
            {getMonth(date)}
        </div>
    }
}

export default AppCalendar