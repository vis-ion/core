
// #region Modules
import React, {
    useState,
    useEffect,
    useRef,
    useReducer
} from 'react'
// #endregion

// #region Components
import {
    Iframe
} from 'components'
// #endregion

// #region Assets & Data
// #endregion

export const AppSpotify = (props) => {
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
    }, [])

    const hideLoader = () => setIsLoading(false)

    const url = 'https://open.spotify.com/'

    return (
        <div>
            <Iframe
                onLoad={ hideLoader }
                width="100%"
                height={ props.height || '50%' }
                scrolling="no"
                frameBorder="no"
                allow="autoplay"
                src={url}
            />
        </div>
    )
}

export default AppSpotify
