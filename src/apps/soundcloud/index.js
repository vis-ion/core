import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input, SoundCloud } from 'components'
// import { TextField } from '@material-ui/core'

export class AppSoundcloud extends Component {
    static propTypes = {
        onDataChange: PropTypes.func,
    }
    static defaultProps = {
        onDataChange: (data) => console.log('define <AppYoutube/>.onDataChange()'),
        isAutoplay: true,
        isLooping: false,
        duration: 4
    }
    state = {
        url: '',
        videoId: undefined
    }
    componentDidMount() {
        console.log('data:', this.props.data)
        const { duration, onNext } = this.props
        this.timeout = setTimeout(onNext, duration * 1000)
        this.getVideoId(this.props.data.url)
        this.setState({
            url: this.props.data.url,
        })
    }
    componentWillUnmount() {
        clearTimeout(this.timeout)
    }
    getVideoId = (url) => {
        const urlParts = url.split('?v=')
        const videoId = urlParts[urlParts.length - 1]
        const data = { url, videoId }
        console.log(`getVideoId: `, data)
        this.setState({ ...data })
        this.props.onDataChange(data)
    }
    render () {
        const { onChange, data, isEditable } = this.props
        const { url, videoId } = this.state

        return <div style={{
            width: '100%',
            overflow: 'hidden'
        }}>
            {
                isEditable && <Input
                label='URL'
                type='text'
                value={url}
                style={{
                    width: '100%',
                }}
                // onChange={(e) => this.getVideoId(e.target.value)}
            />
            }
            <br />
            {
                url && <div className=''>
                    {/* <Soundcloud url={ data.url } /> */}
                    <SoundCloud url={ data.url } trackId={ data.trackId } />
                </div>
            }
        </div>
    }
}

export default AppSoundcloud
