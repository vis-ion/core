import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class AppCountdown extends Component {
    static propTypes = {
        // data: PropTypes.object.isRequired,
        isEditable: PropTypes.bool,
        duration: PropTypes.number,
        onNext: PropTypes.func,
    }
    static defaultProps = {
        duration: 2
    }
    state = {

    }
    componentDidMount() {
        const { duration, onNext } = this.props
        setTimeout(onNext, duration)
    }
    render () {
        return <div>
            <h1>App: Countdown</h1>
        </div>
    }
}

export default AppCountdown