import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import AppMenu from './components/app-menu'
import Navbar from './components/navbar'
import NotFound from './pages/not-found'
// Web:
import Home from './pages/web/home'
// App:
import DemoApps from './pages/apps'
import DemoCharts from './pages/charts'
import DemoComponents from './pages/components'
import DemoUtils from './pages/utils'
import DemoPages from './pages/auth'
// Demo: Other
import './styles/index.scss'
import './favicon.ico'
import pkg from '../package.json'

console.clear()
console.log(`${pkg.name}: v${pkg.version}`)

export class App extends Component {
    state = {
        isAppMenuOpen: false
    }

    toggleAppMenu = () => this.setState({ isAppMenuOpen: !this.state.isAppMenuOpen })
    render () {
        const { isAppMenuOpen } = this.state
        return <Router>
            <div className='fullscreen'>
                <div className='layout'>
                    <Navbar onToggle={ this.toggleAppMenu } />
                    <AppMenu
                        appName={'core.vis-ion'}
                        appVersion={ pkg.version }
                        isOpen={ isAppMenuOpen }
                        onToggle={ this.toggleAppMenu }
                        socialAccount={{
                            instagram: 'http://www.instagram.com',
                            youtube: 'http://www.youtube.com',
                            github: 'http://www.github.com'
                        }}
                    />
                    <Switch>
                        <Route
                            exact
                            path={'/'}
                            render={ () => <Redirect from='/' to={'/home'} /> }
                        />
                        <Route
                            exact
                            path={'/core'}
                            component={ Home }
                        />
                        {/* Demos */}
                        <Route path='/home' component={ Home } />
                        <Route path='/core/apps' component={ DemoApps } />
                        <Route path='/core/pages' component={ DemoPages } />
                        <Route path='/core/charts' component={ DemoCharts } />
                        <Route path='/core/components' component={ DemoComponents } />
                        <Route path='/core/utils' component={ DemoUtils } />
                        {/* Other */}
                        <Route path='*' component={ NotFound } />
                    </Switch>
                </div>
            </div>
        </Router>
    }
}

export default App
