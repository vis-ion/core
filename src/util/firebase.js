import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: 'AIzaSyC-jjX9f71GjxEy-potKYFaQJg8kAsY7GA',
    authDomain: 'vision-identity.firebaseapp.com',
    databaseURL: 'https://vision-identity.firebaseio.com',
    projectId: 'vision-identity',
    storageBucket: 'vision-identity.appspot.com',
    messagingSenderId: '1091015811145',
    appId: '1:1091015811145:web:30bd18e335564b5be65f98',
    measurementId: 'G-2ZMW0R12GQ'
}
firebase.initializeApp(firebaseConfig)

export const AUTH = firebase.auth()
export const DB = firebase.database()
export const STORAGE = firebase.storage()

export const FIREBASE = (endpoint, id = undefined) => {
    console.log('firebase: ', endpoint, id)
    const ref = DB.ref(`/${endpoint}${id
        ? `/${id}`
        : ''}`)
    // debugger
    return ref
}

// AUTH

// - login
export const emailAuthLogin = (email, password) => {
    return AUTH.signInWithEmailAndPassword(email, password)
}
// - register
export const emailAuthRegister = (email, password) => {
    return AUTH.createUserWithEmailAndPassword(email, password)
}
// - logout
export const logOut = () => {
    AUTH.signOut()
}
