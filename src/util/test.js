const currentEnv = process.env.ENVIRONMENT
process.env.ENVIRONMENT = 'TEST'

require('./stats');
require('./saturate');

process.env.ENVIRONMENT = currentEnv