const stats = require('./stats')

const getColumnData = (data, valueKeys) => valueKeys.map(valueKey => data.items.map(row => row[valueKey]))

const withStats = (data, indexKeys, valueKeys) => {
    let columnStats = {}
    const columnData = getColumnData(data, valueKeys)

    columnData.forEach((c, cI) => {
        columnStats[valueKeys[cI]] = stats(c)
    })

    return {
        stats: columnStats,
        ...data
    }
}

const withDimensions = (data, indexKeys, valueKeys) => {
    const keys = (data && data.items && Object.keys(data.items[0])) || []
    return {
        dimensions: {
            keys,
            indexKeys,
            valueKeys
        },
        ...data,
    }
}

const withAll = (data) => {
    const isObject = data && typeof data === 'object'
    if(!isObject && !Array.isArray(data)) {
        return data
    }
    const keys = isObject && Object.keys(data[0])
    const valueKeys = keys.filter(key => !isNaN(data[0][key]))
    const indexKeys = keys.filter(key => !valueKeys.includes(key))

    console.log('withAll, ', { keys, indexKeys, valueKeys })

    return withStats(
        withDimensions(
            { items: data },
            indexKeys,
            valueKeys
        ),
        indexKeys,
        valueKeys
    )
}

// TODO: categorical ...
const withDistribution = (data, valueKeys) => {
    const columnData = getColumnData(data, valueKeys)
}

export default {
    withAll,
    withStats,
    withDimensions,
}

// TEST:
if(process && process.env && process.env.ENVIRONMENT === 'TEST') {
    const data1D = require('../demo/mock-data/1d')
    console.log('_test_:util/saturate.js => withAll(1d)\n', withAll(data1D.items))
}