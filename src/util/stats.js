const stats = (data, options = {
    n: true,
    avg: true,
    min: true,
    max: true,
    sum: true,
    median: true,
    stdDev: false,
    variance: false,
    lowQuart: false,
    uppQuart: false
}) => {

    const isArray = Array.isArray(data)
    if (!isArray) {
        console.warn('STATS_ERR: data is not an Array:', { data })
        return
    }
    if(isArray) {
        const is2DArray = data.length > 0 && Array.isArray(data[0])
        if(is2DArray) {
            // recursive call on each element here
            const subStats = data
                .map((frame, i) => stats(frame, options))

            const totalN = subStats.reduce((prev,curr, index) => curr.n + prev.n)
            return {
                n: totalN,
                min: Math.min(...subStats.map(stat => stat.min)),
                max: Math.max(...subStats.map(stat => stat.max)),
                sum: subStats.reduce((prev, curr, index) => curr.sum + prev.sum),
                median: 0 // TODO: sort this out
            }

        } else {
            const n = data.length
            return {
                n,
                max: Math.max(...data),
                min: Math.min(...data),
                // avg: Math.avg(data),
                sum: data.reduce((prev, curr, index) => curr + prev),
                median: data[n / 2]
            }
        }
    }
}


// TEST:
if(process && process.env && process.env.ENVIRONMENT === 'TEST') {
    const data1D = [
        30, 60, 40, 70
    ]

    const data2D = [
        [30, 60, 40, 70],
        [1, 30, 60, 40, 111, 230]
    ]

    console.log('_test_:util/stats.js => stats(1D):\n', stats(data1D))
    console.log('_test_:util/stats.js => stats(2D):\n', stats(data2D))
}

module.exports = stats