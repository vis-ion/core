const byField = (key) => (a, b) => a > b
    ? 1
    : a < b
        ? -1
        : 0

export default {
    byField
}