// #region Modules
import React, {
  useState,
  useEffect,
  useRef,
  useReducer
} from 'react'
// #endregion

// #region Components
import {
  Button,
  Card,
  Input
} from 'components'
// #endregion

// #region Assets & Data
// #endregion

export const AuthForm = (props) => {
  useEffect(() => {
  }, [])

  const onChange = field => e => {
    props.onChange({ [field]: e.target.value })
  }
  return (
          <div
              className="flex-col"
              style={{
                padding: '4rem',
                margin: 'auto',
                maxWidth: '30rem'
              }}
          >
            <Card style={{ padding: '2rem', backgroundColor: 'rgba(150,150,150, 0.1)' }}>
                {
                    (props.fields || []).map(field => (
                      <div className="flex-col">
                        <div>{ field.label }</div>
                        <Input
                            type={field.type}
                            onChange={onChange(field.key)}
                        />
                        <br />
                      </div>
                    ))
                }
                <br />
                <Button
                    type="submit"
                    onClick={props.onSubmit}
                    className="primary colors white full-width"
                >
                    { props.submitText }
                </Button>
          </Card>
      </div>
  )
}

export default AuthForm
