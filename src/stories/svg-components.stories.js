import * as d3 from 'd3'
import React from 'react'
import { storiesOf } from '@storybook/react'
import { ChartWrapper } from '../charts/wrapper'
import { SvgElement } from '../components/svg/element'

storiesOf('D3 | 2 Components / Gradients', module)
    .add('Linear', () => <ChartWrapper>
        {({ container }) => {
            const svg = d3.select(container).append('svg')
            .attr('width', 800)
            .attr('height', 300)
        }}
    </ChartWrapper>)

storiesOf('D3 | 2 Components / React', module)
    .add('SvgElement', () => <div style={{ display: 'xflex', flexDirection: 'row'}}>
        <SvgElement
            type='text'
            text={'Just another test message'}
            attribs={{
                x: 0,
                y: 16,
                width: 120,
                height: 60,
                fill: 'red'
            }}
        />
        <SvgElement
            type='text'
            text={'Testing more here'}
            attribs={{
                x: 0,
                y: 16,
                width: 120,
                height: 60,
                fill: 'blue'
            }}
        />
        <SvgElement
            type='rect'
            text={'Testing more here'}
            attribs={{
                x: 0,
                y: 16,
                width: 100,
                height: 60,
                fill: 'blue'
            }}
        />
        <SvgElement
            type='line'
            width={64}
            height={32}
            attribs={{
                x1: 0,
                x2: 64,
                y1: 0,
                y2: 32,
                stroke: 'red',
                'stroke-width': 4,
                'stroke-dash-array': '0,0'
            }}
        />
        <SvgElement
            type='circle'
            width={ 64 }
            height={ 64 }
            attribs={{
                cx: 32,
                cy: 32,
                r: 32,
                fill: 'purple'
            }}
        />
    </div>)
