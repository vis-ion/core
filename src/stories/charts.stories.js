import * as d3 from 'd3'
import React from 'react'
import { storiesOf } from '@storybook/react'
import { BarChart, PieChart, LineChart } from 'charts'
import barData from '../demo/mock-data/1d'
import barData2d from '../demo/mock-data/2d'

const galleryLink = 'https://github.com/d3/d3/wiki/Gallery'

storiesOf('D3 | 3 Charts / Pie', module)
    .add('Default', () => <PieChart
        data={ barData }
    />)

storiesOf('D3 | 3 Charts / Bar', module)
    .add('Default', () => <BarChart
        data={ barData }
    />)
    .add('2D', () => <BarChart
        isStacked
        data={ barData2d }
    />)

storiesOf('D3 | 3 Charts / Line')
    .add('Default', () => <LineChart
        data={ barData }
    />)

