import React, { Component } from 'react'
import SplitPane from 'react-split-pane'

export class Layout extends Component {
    static defaultProps = {
        defaultSize: 150
    }

    render () {
        const { leftDiv, rightDiv, defaultSize } = this.props

        return <SplitPane
            split='vertical'
            defaultSize={ defaultSize }
            minSize={ 55 }
            maxSize={ 400 }
            style={{
                position: 'relative',
                height: '100vh'
            }}
        >
            { leftDiv && <div className='layout-panel' style={{ color: '#888' }}>{ leftDiv }</div> }
            { rightDiv && <div className='layout-panel'>{ rightDiv }</div> }
        </SplitPane>
    }
}

export default Layout
