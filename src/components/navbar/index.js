import React, { Component } from 'react'
import { withRouter } from 'react-router'
import FAIcon from 'react-fontawesome'
import visionLogo from 'img/vision-logo-2-blue.png'

export class Navbar extends Component {
    static defaultProps = {
        onToggle: () => console.log('<Navbar>.onToggle UNDEFINED'),
        theme: 'light'
    }

    componentDiDMount () {
        console.log('<Navbar /> :', window.location.pathname)
    }

    goTo = (path) => () => {
        this.props.history.push('/core/' + path)
    }

    render () {
        const { onToggle, theme } = this.props
        const { pathname } = window.location
        const items = [
            {
                label: 'Components',
                path: 'components',
                faIcon: 'th'
            },
            {
                label: 'Apps',
                path: 'apps',
                faIcon: 'cubes'
            },
            {
                label: 'Pages',
                path: 'pages',
                faIcon: 'file'
            },
            {
                label: 'Charts',
                path: 'charts',
                faIcon: 'chart-pie'
            },
            { separator: true },
            {
                label: 'Utils',
                path: 'utils',
                faIcon: 'cogs'
            }
        ]

        const bottomItems = [
            // {   separator: true },
            {
                label: 'Help',
                path: 'help',
                faIcon: 'question-circle'
            },
            // {   separator: true },
            {
                label: 'Settings',
                path: 'settings',
                faIcon: 'cog'
            }
        ]

        const isActiveTab = (name) => pathname.split('/').pop() === name

        return <div className={`navbar ${theme} space-between`}>
            <div>
                <div className='item' onClick={ onToggle }>
                    <img src={ visionLogo } />
                </div>
                {
                    items.map((item, i) => item.separator
                        ? <div key={ i }
                            style={{ width: '100%', height: '1px', backgroundColor: 'lightgrey' }}
                        />
                        : <div key={ i }
                            title={ item.label }
                            className={`item ${isActiveTab(item.path) ? 'active' : ''}`}
                            onClick={ this.goTo(item.path) }>
                            {
                                item.faIcon
                                    ? <FAIcon name={ item.faIcon } />
                                    : item.label
                            }
                        </div>
                    )
                }
            </div>
            <div>
                {
                    bottomItems.map((item, i) => item.separator
                        ? <div key={ i }
                            style={{ width: '100%', height: '1px', backgroundColor: 'lightgrey' }}
                        />
                        : <div key={ i }
                            title={ item.label }
                            className={`item ${isActiveTab(item.path) ? 'active' : ''}`}
                            onClick={ this.goTo(item.path)}>
                            {
                                item.faIcon
                                    ? <FAIcon name={ item.faIcon } />
                                    : item.label
                            }
                        </div>
                    )
                }
            </div>
        </div>
    }
}

export default withRouter(Navbar)
