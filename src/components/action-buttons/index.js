import React from 'react'
import FAIcon from 'react-fontawesome'
export const ActionButtons = ({ onDelete, onCancel, onSave }) => {
    const buttonStyle = {
        // paddingLeft: '0.75rem',
        // paddingRight: '0.75rem',
        marginRight: '0.25em',
        width: '3em',
        height: '3em',
        borderColor: 'transparent'
    }
    const iconStyle = {
        // marginLeft: '0.2em',
        // marginRight: '0.2em',
        margin: 'auto'
    }
    return <div className='flex-row'>
        {
            onSave && <button
                className='success hollow round flex-row action-button'
                style={ buttonStyle }
                onClick={ onSave }
                title='Save'
            >
                <div className='icon'><FAIcon name='check-circle' /></div>
            </button>
        }
        {
            onCancel && <button
                className='warning hollow round flex-row action-button'
                style={ buttonStyle }
                onClick={ onCancel }
                title='Cancel'
            >
                <div className='icon'><FAIcon name='minus-circle' /></div>
            </button>
        }
        {
            onDelete && <button
                className='error hollow round flex-row action-button'
                onClick={ onDelete }
                title='Delete'
            >
                <div className='icon'><FAIcon name='times-circle' /></div>
            </button>
        }
    </div>
}
