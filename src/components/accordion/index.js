import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import PropTypes from 'prop-types'

export class Accordion extends Component {
    static propTypes = {
        isOpen: PropTypes.bool,
        isOpenDefault: PropTypes.bool,
        title: PropTypes.any,
        children: PropTypes.any
    }

    static defaultProps = {
        title: 'Accordion.title',
        isOpen: true
    }

    state = {
        isOpen: true
    }

    componentDidMount () {
        this.setState({
            isOpen: this.props.isOpenDefault
        })
    }

    toggle = () => this.setState({ isOpen: !this.state.isOpen })
    render () {
        const { children, title, onClose, onAdd } = this.props
        const { isOpen } = this.state
        const buttonStyle = { marginTop: '1rem', marginRight: '1rem' }
        // return <div className={`accordion`}>
        return <div
            className={`accordion ${isOpen ? 'accordion-open' : ''}`}>
            <div
                className='flex-row space-between title'
                style={{ width: '100%', height: '3rem', lineHeight: '3rem' }}>
                <span style={{ cursor: 'pointer', marginLeft: '1rem' }}>
                    {
                        onAdd && <FAIcon name='plus' style={{ marginTop: '1rem', marginRight: '1rem' }} onClick={ onAdd } />
                    }
                    &nbsp;
                    <span onClick={this.toggle}>{ title }</span>
                </span>
                <span>
                    <span onClick={this.toggle}>{
                        isOpen
                            ? <FAIcon name='chevron-up' style={buttonStyle} />
                            : <FAIcon name='chevron-down' style={buttonStyle} />
                    }
                    </span>
                    {
                        onClose && <FAIcon name='times' style={{ marginTop: '1rem', marginRight: '1rem' }} onClick={ onClose } />
                    }
                </span>
            </div>
            {/* <hr style={{ position: 'relative', bottom: '1px' }} /> */}
            { isOpen && <div className='content'>{ children }</div> }
        </div>
    }
}

export default Accordion
