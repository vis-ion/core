import React, { Component } from 'react'

export class Button extends Component {
    render () {
        const {
            children,
            className,
            color,
            ...rest
        } = this.props

        return (
            <button
                { ...rest }
                className={`vision-button ${
                    color
                        ? ' ' + color
                        : ''}${
                    className
                        ? ' ' + className
                        : ''}
                        colors white`}>
                { children }
            </button>
        )
    }
}

export default Button
