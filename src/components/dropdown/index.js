import React, { Component } from 'react'

export class Dropdown extends Component {
    render () {
        const { options } = this.props
        return <select className='vision-select'>
            {
                (options || []).map((option, o) => (
                    <option key={ o } value={ option.value }>
                        { option.label }
                    </option>
                ))
            }
        </select>
    }
}

export default Dropdown
