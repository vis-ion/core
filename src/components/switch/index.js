import React, { Component } from 'react'

export class Switch extends Component {
    static defaultProps = {
        onChange: e => console.log(`<Switch />.onChange UNDEFINED`, e),
        isLabelRight: true
    }
    render () {
        const { onChange, label, isLabelRight } = this.props
        return <div className='vision-switch flex-row'>
            { !isLabelRight && <div>{ label }</div> }
            &nbsp;
            <label>
                <input type='checkbox' onChange={ onChange }/>
                <span className='slider'></span>
            </label>
            &nbsp;
            { isLabelRight && <div>{ label }</div> }
        </div>
    }
}

export default Switch
