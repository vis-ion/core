import React, { Component } from 'react'
// import { connect } from 'react-redux'
// import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import ClickOutside from 'react-click-outside'
// import { DropMenu } from 'livesign-ui'
import FAIcon from 'react-fontawesome'
// import { setVideoModal } from 'data/session/actions'
// import storage from 'util/storage'

const Button = ({ children, ...rest }) => <button { ...rest }>{ children }</button>
const Card = ({ children }) => <div className='card'>{ children }</div>
const DropMenu = Card
const TextField = (props) => <input style={{ width: '100%' }} { ...props } />
const Icon = FAIcon

// const mapActionsToProps = dispatch => bindActionCreators({
//     setVideoModal
// }, dispatch)
// const mapStateToProps = state => ({
// })
export class CollectionView extends Component {
    static propTypes = {
        hasAddButton: PropTypes.bool,
        collectionName: PropTypes.string.isRequired,
        fabItems: PropTypes.array,
        items: PropTypes.array.isRequired,
        GridItem: PropTypes.func.isRequired,
        ListItem: PropTypes.func.isRequired,
        MapView: PropTypes.element,
        selectItem: PropTypes.func,
        createItem: PropTypes.func,
        options: PropTypes.array
    }

    static defaultProps = {
        hasAddButton: true,
        options: [],
        item: [],
        fabItems: [],
        selectItem: (i) => () => {}
    }

    state = {
        viewType: 'list',
        items: [],
        openMenuIndex: -1
    }

    setView = (viewType) => () => {
        const { collectionName } = this.props
        this.setState({
            viewType
        })
        // const collectionView = storage.local.get('collectionView')
        // collectionView[collectionName] = viewType
        // storage.local.set('collectionView', collectionView)
    }

    componentDidMount () {
        const { collectionName, items } = this.props
        // const collectionView = storage.local.get('collectionView') || storage.local.set('collectionView', {})
        const viewType = 'grid' // collectionView && collectionView[collectionName] || 'grid'

        console.log(viewType)

        this.setState({
            viewType,
            items
        })
    }

    filter = (e) => {
        e.preventDefault()
        const query = e.target.value
        this.setState({
            query,
            items: this.props.items.filter(item => item.name.toLowerCase().includes(query.toLowerCase()))
        })
    }

    openMenu = (index) => () => this.setState({ openMenuIndex: index })
    closeMenu = () => this.setState({ openMenuIndex: -1 })

    openVideoModal = () => {
        this.props.setVideoModal(true)
    }

    render () {
        const {
            viewType,
            items,
            query,
            openMenuIndex
        } = this.state

        const {
            MapView,
            GridItem,
            ListItem,
            collectionName,
            iconName,
            fabItems,
            selectItem,
            createItem,
            options
        } = this.props

        const EmptyState = <div className='empty-state'>
            <Card className='padded content'>
                { iconName !== undefined
                    ? <span className='icon'>
                        <Icon name={iconName}/>
                        <br />
                    </span>
                    : null
                }
                {`You have no ${collectionName}s, would you like to create one?`}
                <br />
                {/* { createItem && <Button
                color='primary'
                variant='contained'
                onClick={createItem}>Create {collectionName}</Button>
            } */}
            </Card>
        </div>

        const OptionsButton = (index) => <div className='options-button' onClick={this.openMenu(index)}>
            <Icon name='ellipsis-v'/>
        </div>

        const NoMatchesView = <div className='align-center'>
            <Card className='padded content'>
                <h1>No Match</h1>
                <h3>There are no {collectionName} matching  <em><strong>"{query}"</strong></em></h3>
            </Card>
        </div>

        return <div className={'collections-container'}>
            {
                this.props.items.length > 0 && <div>
                    <div className='search-field'>
                        <TextField
                            type='text'
                            label='search'
                            style={{ width: '100%' }}
                            value={query}
                            onChange={this.filter}
                        />
                    </div>
                    <div className='flex-row view-controls'>
                        {
                            ListItem && <Icon
                                name={ 'list' }
                                onClick={this.setView('list')}
                                className={`${viewType === 'list' ? 'active-view' : ''}`}
                            />
                        }
                        {
                            GridItem && <Icon
                                name={ 'grip-horizontal' }
                                onClick={this.setView('grid')}
                                className={`${viewType === 'grid' ? 'active-view' : ''}`}
                            />
                        }
                        {
                            MapView && <Icon
                                name={ 'map' }
                                onClick={this.setView('map')}
                                className={`${viewType === 'map' ? 'active-view' : ''}`}
                            />
                        }
                    </div>
                </div>
            }
            {
                viewType === 'map' && MapView
            }
            {
                ['list', 'grid'].includes(viewType) && items.length > 0 &&
                    <div className={`${viewType}`}>
                        {
                            (items || []).map((item, i) => {
                                return <Card
                                    key={`${collectionName}-${viewType}-item-${i}`}
                                    className={`collection-${viewType}-item`}
                                    onClick={ selectItem(item) }
                                >
                                    {
                                        viewType === 'list'
                                            ? <span className='flex-row'>
                                                { ListItem(item) }
                                                {/* { options && OptionsButton(i) } */}
                                            </span>
                                            : <span className='flex-row'>
                                                { GridItem(item) }
                                                {/* { options && OptionsButton(i) } */}
                                            </span>
                                    }
                                    <br />
                                    <br />
                                    {
                                        i === openMenuIndex && <ClickOutside onClickOutside={this.closeMenu}>
                                            <DropMenu align='right'>
                                                {
                                                    options.map((option, j) => <li
                                                        key={ `collection-view-${collectionName}-menuitem-${j}` }
                                                        onClick={ () => {
                                                            option.onClick(item)
                                                            this.setState({ openMenuIndex: -1 })
                                                        }}>
                                                        <span className='flex-row'>
                                                            <Icon name={option.iconName} />
                                                            &nbsp;&nbsp;
                                                            {option.label}
                                                        </span>
                                                    </li>)
                                                }
                                            </DropMenu>
                                        </ClickOutside>
                                    }
                                </Card>
                            })
                        }
                    </div>
            }
            {
                items.length == 0
                    ? this.props.items.length > 0
                        ? NoMatchesView
                        : EmptyState
                    : null
            }
            {
                // fabItems && <FAB
                //     items={ fabItems }
                //     openVideoModal={ this.openVideoModal }
                // />
            }
            {/* {
                <div
                    onClick={createItem}
                    className={'add-new-collection-item-button'}>
                    <Icon name='plus' />
                </div>
            } */}
        </div>
    }
}

export default CollectionView

// export default connect(mapStateToProps, mapActionsToProps)(CollectionView)
