import React from 'react'
import FAIcon from 'react-fontawesome'

export const EditInline = ({
    value,
    onChange
}) => {
    return <div className='edit-inline'>
        <div className='title'
            contentEditable
            suppressContentEditableWarning={true}
            // onInput={ this.updateSlideshow('name') }
            // onBlur={ this.updateSlideshow('name') }
        >{ value }</div>
        <div className='icon'><FAIcon name='pen' /></div>
    </div>
}
