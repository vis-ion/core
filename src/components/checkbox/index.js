import React, { Component } from 'react'

export class Checkbox extends Component {
    static defaultProps = {
        onChange: e => console.log('<Checkbox />.onChange UNDEFINED', e),
        isLabelRight: true
    }

    render () {
        const { placeholder, checked, value, onChange, label, isLabelRight } = this.props

        return <label>
            { !isLabelRight && <span>{ label }</span> }
            <input
                type='checkbox'
                checked={ checked }
                onChange={ onChange }
                value={ value }
            />
            { isLabelRight && <span>{ label }</span> }
        </label>
    }
}

export default Checkbox
