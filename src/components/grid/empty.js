import React from 'react'
import FAIcon from 'react-fontawesome'

export const Empty = ({ onClick, style }) => {
    return <div style={{
        position: 'relative',
        width: 'calc(100% - 4px)',
        height: (style && style.height) || 'calc(100% - 4px)',
        cursor: 'grab',
        backgroundColor: '#eeeef3',
        border: '2px dotted #ccc'
    }}>
        <button
            onClick={ onClick }
            className='round'
            style={{
                borderRadius: '2rem',
                border: 'none',
                boxShadow: '0px 5px 5px 1px lightgrey',
                display: 'block',
                width: '3rem',
                height: '3rem',
                margin: 'auto',
                position: 'relative',
                top: 'calc(50% - 1.5rem)',
                outline: 'none',
                fontSize: '1rem',
                cursor: 'pointer',
                color: '#00adee',
                backgroundColor: 'rgba(white, 0.8)'
            }}>
            <FAIcon name='pen' />
        </button>
        <div style={{
            position: 'absolute',
            bottom: '0.5rem',
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            color: 'grey',
            fontSize: '0.8rem'
        }}>
            <div className='flex-row' style={{ marginLeft: '0.5rem'}}>
                <FAIcon name='arrows-alt' rotation={ 60 } />
                {/* <div>&nbsp;<b>move</b>&nbsp;me...</div> */}
            </div>
            <br />
            <div className='flex-row' style={{marginRight: '0.5rem'}}>
                <b>resize</b>&nbsp;
                {/* <FAIcon name='share' rotation={ 90 } /> */}
            </div>
        </div>
    </div>
}

Empty.type = 'grid:empty'

export default Empty
