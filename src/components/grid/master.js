import React, { Component } from 'react'
import FAIcon from 'react-fontawesome'
import MasterChart from 'charts/master'
import AppMaster from 'apps/master'
import saturate from 'util/saturate'
import Empty from './empty'
import AddTile from './add-tile'

import RGL, { WidthProvider } from 'react-grid-layout'
const ReactGridLayout = WidthProvider(RGL)

export class MasterGrid extends Component {
    static defaultProps = {
        items: [],
        onClickItem: (i) => console.log(`UNDEFINED: <MasterGrid/>.onClickItem = (i) => {}`),
        onClearItem: (i) => console.log(`UNDEFINED: <MasterGrid/>.onClearItem = (i) => {}`),
        onChangeLayout: (layout) => console.log(`UNDEFINED: <MasterGrid/>.onChangeLayout = () => {}`)
    }
    state = {
        dataSources: [],
        allSourceData: [],
        allMappedData: []
    }
    clearTile = i => () => this.props.onClearItem(i)
    deleteTile = i => () => this.props.onDeleteItem(i)

    editTile = i => () => {
        const tileTypes = [
            ...AppMaster.types,
            ...MasterChart.types
        ]
        const type = tileTypes[parseInt(Math.random() * tileTypes.length)]
        this.props.onEditItem(i, type)
    }
    addTile = () => {
        this.props.onAddItem()
    }
    onChangeTile = i => data => {
        const { dataSources } = this.state
        const tile = this.props.items[i]
        if(tile.type === 'app:upload') {
            this.setState({
                dataSources: {
                    ...dataSources,
                    [i]: data,
                },
            }, () => {
                const allSourceData = Object
                    .keys(this.state.dataSources)
                    .map(index => this.state.dataSources[index].files)
                    .flat()

                this.setState({
                    allSourceData,
                    allMappedData: allSourceData.map(frame => {
                        return saturate.withAll(frame.data)
                    })
                }, () => console.log('MasterGrid: ', this.state.allMappedData))
            })
        }

        this.props.onChangeItem(i, data)
    }
    onChangeLayout = layout => {
        this.props.onChangeLayout(layout)
    }

    renderMasterTile = (type, i) => {
        const { items } = this.props
        const isActive = this.props.activeTileIndex === i

        switch(type) {
            case 'grid:empty':
                return <Empty
                    onClick={ this.editTile(i) }
                    style={ isActive && items[i].type !== 'grid:add' ? { height: 'calc(100% - 2rem)'} : undefined }
                />
            case 'grid:add':
                return <AddTile
                    onClick={ this.addTile }
                    style={ isActive && items[i].type !== 'grid:add' ? { height: 'calc(100% - 2rem)'} : undefined }
                />
            default: return null
        }
    }

    render() {
        const {
            items,
            data,
            isExportable,
            activeTileIndex,
            onClickItem,
            DEBUG
        } = this.props
        const {
            allSourceData,
            allMappedData,
            dataSources
        } = this.state
        const buttonStyle = {
            borderRadius: '2rem',
            border: 'none',
            display: 'block',
            width: '2rem',
            height: '2rem',
            lineHeight: '2rem',
            position: 'relative',
            outline: 'none',
            fontSize: '0.8rem',
            cursor: 'pointer',
            color: '#888',
            backgroundColor: 'transparent'
        }
        const activeTile = activeTileIndex >= 0 && this.props.length >= 0 && this.props.items[activeTileIndex]
        const hasType = activeTile && activeTile.type

        const TileControls = ({ index }) => <div className='flex-row space-between' style={{
            position: 'absolute',
            top: '0rem',
            left: '0rem',
            width: 'calc(100% - 0rem)',
            height: '2rem',
            lineHeight: '2rem',
            backgroundColor: '#00adee',
            color: 'white',
        }}>
            <div className='flex-row'>
                <button style={{ ...buttonStyle, color: 'inherit' }} onClick={ this.editTile(index) }>
                    <FAIcon name='pen' />
                </button>
                <div style={{ display: 'block' }}>{ items[index].type }</div>
            </div>
            <div className='flex-row'>
                <button style={{ ...buttonStyle, color: 'inherit' }} onClick={ this.clearTile(index) }>
                    <FAIcon name='broom' />
                </button>
                <button style={{ ...buttonStyle, color: 'inherit' }} onClick={ this.deleteTile(index) }>
                    <FAIcon name='times' />
                </button>
            </div>
        </div>

        DEBUG && console.log(items, this.props)

        return (
            <ReactGridLayout
                cols={48}
                rowHeight={12}
                width={1200}
                // compactType={ 'vertical' }
                preventCollision={false}
                onLayoutChange={this.onChangeLayout}
            >
            {
                items.map((tile, i) => {
                    const isActive = activeTileIndex === i
                    return <div key={i}
                        className={`grid-tile ${isActive ? 'active-tile' : ''}`}
                        data-grid={tile.layout}
                        onClick={ onClickItem(i) }
                        >
                        {
                            tile.type && tile.type.includes('chart:')
                                ? <MasterChart
                                    index={ i }
                                    type={ tile.type }
                                    indexKeys={ allMappedData.indexKeys || [] }
                                    valueKeys={ allMappedData.valueKeys || [] }
                                    options={{
                                        ...tile.options,
                                        getValue: (d) => d[(((allMappedData.length && allMappedData[0]) || {}).valueKeys)[0]]
                                    }}
                                    data={ (allMappedData && allMappedData[0]) ||  tile.data || data }
                                    gridDataAll={ allSourceData }
                                    gridDataTile={ dataSources[i] }
                                    isEditing={ isActive }
                                    isExportable={ isExportable }
                                    onChange={ this.onChangeTile(i) }
                                    onEdit={ this.editTile(i) }
                                    onClose={ this.editTile(i) }
                                />
                                : tile.type && tile.type.includes('app:')
                                    ? <AppMaster
                                        index={ i }
                                        type={ tile.type }
                                        options={ tile.options }
                                        data={ tile.data }
                                        gridDataAll={ allSourceData }
                                        gridDataTile={ dataSources[i] }
                                        isEditing={ isActive }
                                        isExportable={ isExportable && false }
                                        onChange={ this.onChangeTile(i) }
                                        onEdit={ this.editTile(i) }
                                        onClose={ this.editTile(i) }
                                    />
                                    : tile.type && tile.type.includes('grid:')
                                        ? this.renderMasterTile(tile.type, i)
                                        : <Empty
                                            onClick={ this.editTile(i) }
                                            style={ isActive && items[i].type !== 'grid:add' ? { height: 'calc(100% - 2rem)'} : undefined }
                                        />
                        }
                        { isActive && items[i].type !== 'grid:add' && <TileControls index={ i } /> }
                    </div>
                })
            }
            </ReactGridLayout>
        )
    }
}

MasterGrid.types = [
    ...MasterChart.types,
    ...AppMaster.types
]

MasterGrid.options = [
    ...MasterChart.options,
    ...AppMaster.options
]

export default MasterGrid
