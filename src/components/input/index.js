import React from 'react'
import PropTypes from 'prop-types'

export const Input = ({ children, className, color, ...rest }) => (
    <input
        className={`vision-input${
            color
                ? ' ' + color
                : ''
        }${
            className
                ? +' ' + className
                : ''
        }`}
        { ...rest }
    />
)

export default Input
