import React, { Component } from 'react'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import FAIcon from 'react-fontawesome'
import visionLogo from 'img/vision-logo-2-blue.png'

const supportedPlatforms = {
    blogger: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Blogger-01-256.png',
        url: ''
    },
    facebook: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Facebook-01-256.png',
        url: 'http://www.facebook.com'
    },
    github: {
        logo: 'https://cdn3.iconfinder.com/data/icons/popular-services-brands/512/github-512.png',
        url: 'http://www.github.com'
    },
    instagram: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Instagram-01-512.png',
        url: 'http://www.instagram.com'
    },
    linkedin: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Linked_In-01-256.png',
        url: 'http://www.linkedin.com'
    },
    pinterest: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Pinterest-01-256.png',
        url: ''
    },
    skype: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Skype-01-256.png',
        url: ''
    },
    snapchat: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Snapchat-01-256.png',
        url: ''
    },
    soundcloud: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Google_Plus-01-256.png',
        url: 'https://soundcloud.com/bilo-midnite'
    },
    spotify: {
        logo: 'https://cdn3.iconfinder.com/data/icons/social-media-2068/64/_spotify-512.png',
        url: 'https://open.spotify.com/search/'
    },
    twitter: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Twitter-01-256.png',
        url: 'http://www.twitter.com'
    },
    vimeo: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/Vimeo-01-256.png',
        url: ''
    },
    youtube: {
        logo: 'https://cdn4.iconfinder.com/data/icons/social-media-2110/64/YouTube-01-256.png',
        url: 'http://www.youtube.com'
    },
    deviantart: {
        logo: 'https://images.vexels.com/media/users/3/137250/isolated/preview/faeefae02621730a25cb072755c6483a-deviantart-icon-logo-by-vexels.png',
        url: ''
    }
}
export class AppMenu extends Component {
    static defaultProps = {
        socialAccount: {},
        appName: 'App Name',
        appVersion: 'v0.0.1',
        onToggle: () => console.log('<AppMenu>.onToggle: UNDEFINED'),
        theme: 'light'
    }

    goTo = path => () => {
        this.props.onToggle()
        this.props.history.push(path)
    }

    render () {
        const {
            appName,
            appVersion,
            socialAccount,
            isOpen,
            onToggle,
            theme
        } = this.props
        const platforms = Object.keys(socialAccount).length > 0 ? socialAccount : supportedPlatforms
        const dividerStyle = {
            width: '16rem',
            margin: 'auto',
            marginTop: '1rem',
            marginBottom: '1rem'
        }
        return <div className={`vision-app-menu ${theme} ${isOpen ? '' : 'hidden'}`}>
            {/* HEADER */}
            <div className='top'>
                <div className='icon' onClick={ onToggle }>
                    <FAIcon name='times' />
                </div>
                <div className='logo'>
                    <img src={ visionLogo } />
                </div>
                <div className='user'>
                    <div className='email'>email@address.com</div>
                    <div className='avatar'>
                        <FAIcon name='user-circle' />
                    </div>
                </div>
            </div>
            {/* BODY */}
            <div className='content'>
                <div className='flex-col'>
                    {/* <img className='logo' src={ visionLogo } /> */}
                    <div className='title logo-font'>{ appName }</div>
                    <div className='version'>{ appVersion }</div>
                </div>
                <div>
                    <div className='link' onClick={ this.goTo('/')}>
                        {/* <FAIcon name='home' />&nbsp;&nbsp; */}
                        Home
                    </div>
                    <div className='divider horizontal' style={ dividerStyle } />
                    <div className='link'>
                        {/* <FAIcon name='project-diagram' />&nbsp;&nbsp; */}
                        Platform
                    </div>
                    <div className='divider horizontal' style={ dividerStyle } />
                    <div className='link'>
                        {/* <FAIcon name='sign-out-alt' />&nbsp; */}
                        {/* <FAIcon name='door-open' />&nbsp;&nbsp; */}
                        Log Out
                    </div>
                </div>
            </div>
            {/* FOOTER */}
            <div className='bottom'>
                <div className='links'>
                    <a href={ window.location.toString() }>Terms</a>
                    <a href={ window.location.toString() }>Privacy</a>
                    <a href={ window.location.toString() }>Contact</a>
                    <a href={ window.location.toString() }>About</a>
                </div>
                <div className='platforms'>
                    <div className='flex-row' style={{ margin: 'auto', flexWrap: 'wrap' }}>
                        {
                            Object.keys(platforms).map((id, i) => <div key={ i } title={ id }>
                                <a target='_blank' href={ platforms[id].url || platforms[id] || supportedPlatforms[id].url }>
                                    <img src={ supportedPlatforms[id].logo } className='icon' />
                                </a>
                            </div>)
                        }
                    </div>
                </div>
                <div className='languages'>
                    <div></div>
                    <select style={{ position: 'absolute', right: '2rem', bottom: '2.5rem' }}>
                        <option>English (UK)</option>
                        <option>English (US)</option>
                        <option>German (DE)</option>
                        <option>French (FR)</option>
                    </select>
                </div>
            </div>
        </div>
    }
}

export default withRouter(AppMenu)
