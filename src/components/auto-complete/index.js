// #region Modules
import React, {
    useState,
    useEffect,
    useRef,
    useReducer
} from 'react'
import PropTypes from 'prop-types'
// #endregion

// #region Components
import { Input } from 'components'
// #endregion

export const AutoComplete = ({
    options,
    filter,
    renderSuggestion
}) => {
    const inputRef = useRef(null)
    const [isOpen, setIsOpen] = useState(true)
    const [query, setQuery] = useState('')
    const [choice, setChoice] = useState(null)
    const [suggestions, setSuggestions] = useState(options)

    useEffect(() => {
    }, [])

    const filterOptions = (searchText) => {
        setSuggestions(suggestions.filter(filter(suggestions, searchText)))
    }

    const search = (e) => {
        const q = e.target.value
        setQuery(q)
        if (q.length === 0) {
            console.log('setting suggestions')
            setSuggestions(options)
            setIsOpen(false)
        } else {
            setSuggestions(filter(suggestions, q))
        }
    }

    const onSelect = (s) => {
        console.log('selected ', s)
        setIsOpen(false)
    }

    return (
        <div className="autocomplete" >
            <Input
                ref={ inputRef }
                value={ query }
                onChange={ search }
                className="aut-input"
            />
            <div className="suggestions">
                {
                    (isOpen || query.length > 0) && suggestions.map(s => renderSuggestion(s, onSelect))
                }
            </div>
        </div>
    )
}

AutoComplete.propTypes = {
    options: PropTypes.array,
    filter: PropTypes.func,
    renderSuggestion: PropTypes.func
}

AutoComplete.defaultProps = {
    options: [],
    filter: (suggestions, searchText) => suggestions
        .filter(s => s.name.toLowerCase()
            .includes(searchText.toLowerCase())
        ),
    renderSuggestion: (s, onSelect) => <div className='auto-suggestion flex-row space-between' onClick={ () => onSelect(s)}>
        { s.name }
    </div>
}
