#!/bin/bash
echo "1. Installing NVM"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

echo "2. Node.js@10: downloading & executing installer"
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

echo "3. Node.js@10: installing"
sude apt install nodejs