#!/bin/bash
clear
echo "
       *.                  *.
      ***                 ***
     *****               *****
    .******             *******
    ********            ********
   ,,,,,,,,,***********,,,,,,,,,
  ,,,,,,,,,,,*********,,,,,,,,,,,
  .,,,,,,,,,,,*******,,,,,,,,,,,,
      ,,,,,,,,,*****,,,,,,,,,.
         ,,,,,,,****,,,,,,
            .,,,***,,,,
                ,*,.
"

echo "Deploying to gitlab pages"
echo "-------------------------"

echo "- stashing changes"
git stash

echo "- checking out master"
git checkout master

echo "- building static folder './artifact'"
yarn build:static

echo "- staging static folder './artifact'"
git add ./artifact
echo "- creating deploy commit"
git commit -m "GITLAB-PAGES-DEPLOY: $(DATE)"

echo "- deploying (push to master)"
git push origin master

echo "- complete"